package com.jeng.framework.activemq.consumer;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class ProxyJMSConsumerTest {

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/spring-activemq.xml");
        ProxyJMSConsumer proxyJMSConsumer = (ProxyJMSConsumer) applicationContext.getBean("messageReceiver");
        proxyJMSConsumer.recive();
        System.out.println("初始化消息消费者");
    }
}