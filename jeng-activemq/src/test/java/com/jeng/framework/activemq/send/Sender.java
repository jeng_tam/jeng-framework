package com.jeng.framework.activemq.send;

import com.jeng.framework.activemq.BaseActivemqTest;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.*;

/**
 * Created by jengt_000 on 2015/2/7.
 */
public class Sender {
    /**
     * @param args
     * jmsTemplate和destination都是在spring配置文件中进行配制的
     * Sender只使用了配置文件中的jmsFactory，jmsTemplate，还有destination这三个属性
     */
    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("config/spring-activemq.xml");
        JmsTemplate template = (JmsTemplate) applicationContext.getBean("jmsTemplate");
        Destination destination = (Destination) applicationContext.getBean("destination");
        /*template.setDeliveryPersistent(true);
        template.setSessionTransacted(true);
        template.setDeliveryMode(DeliveryMode.PERSISTENT);*/
        template.send(destination, new MessageCreator() {
            public Message createMessage(Session session) throws JMSException {
                TextMessage textMessage = session.createTextMessage("发送消息：你的快递已送达，请查收！");
                /*textMessage.setJMSDeliveryMode(DeliveryMode.PERSISTENT);*/
                return textMessage;
            }
        });
        System.out.println("成功发送了一条JMS消息");
    }
}
