package com.jeng.framework.log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;

import org.slf4j.LoggerFactory;

/**
 * The logger slf4j adapter user to implements the com.steven.fremawork.core.common.log.Logger
 * 
 * @author: 		XinZheng Tan 
 * @date: 			Oct 21, 2013 12:41:44 AM 
 *
 */
public class LoggerSlf4jAdapter implements Logger{

	private org.slf4j.Logger logger;
	
	public LoggerSlf4jAdapter(final Class<?> clazz) {
		logger = LoggerFactory.getLogger(clazz);
	}
    @Override
    public void debug(final Class<?> clazz, final String methods, final String message) {
        logger = LoggerFactory.getLogger(clazz);
        logger.debug("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, message);
    }

    @Override
    public void debug(final Class<?> clazz, final String methods, final Exception ex) {
        logger = LoggerFactory.getLogger(clazz);
        StringWriter sw = setPrintStackTrace(ex);
        logger.debug("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, sw.toString());
    }

    @Override
    public void info(final Class<?> clazz, final String methods, final String message) {
        logger = LoggerFactory.getLogger(clazz);
        logger.info("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, message);
    }

    @Override
    public void info(final Class<?> clazz, final String methods, final Exception ex) {
        logger = LoggerFactory.getLogger(clazz);
        StringWriter sw = setPrintStackTrace(ex);
        logger.info("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, sw.toString());
    }

    @Override
    public void error(final Class<?> clazz, final String methods, final String message) {
        logger = LoggerFactory.getLogger(clazz);
        logger.error("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, message);
    }

    @Override
    public void error(final Class<?> clazz, final String methods, final Exception ex) {
        logger = LoggerFactory.getLogger(clazz);
        StringWriter sw = setPrintStackTrace(ex);
        logger.error("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, sw.toString());
    }

    @Override
    public void debug(final String className, final String methods, final String message) {
        logger = LoggerFactory.getLogger(className);
        logger.debug("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, message);
    }

    @Override
    public void debug(final String className, final String methods, final Exception ex) {
        logger = LoggerFactory.getLogger(className);
        StringWriter sw = setPrintStackTrace(ex);
        logger.debug("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, sw.toString());
    }

    @Override
    public void info(final String className, final String methods, final String message) {
        logger = LoggerFactory.getLogger(className);
        logger.info("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, message);
    }

    @Override
    public void info(final String className, final String methods, final Exception ex) {
        logger = LoggerFactory.getLogger(className);
        StringWriter sw = setPrintStackTrace(ex);
        logger.info("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, sw.toString());
    }

    @Override
    public void error(final String className, final String methods, final String message) {
        logger = LoggerFactory.getLogger(className);
        logger.error("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, message);
    }

    @Override
    public void error(final String className, final String methods, final Exception ex) {
        logger = LoggerFactory.getLogger(className);
        StringWriter sw = setPrintStackTrace(ex);
        logger.error("Exceute Time: {}, Method: {}, Message: {}", new Date(), methods, sw.toString());
    }

	@Override
	public void warn(final Class<?> clazzClass, final String methodName, final String message) {
		 logger = LoggerFactory.getLogger(clazzClass);
         logger.error("Exceute Time: {}, Method: {}, Message: {}", new Date(), methodName, message);
	}

	@Override
	public void warn(final Class<?> clazzClass, final String methodName, final Exception exception) {
		logger = LoggerFactory.getLogger(clazzClass);
        StringWriter sw = setPrintStackTrace(exception);
        logger.warn("Exceute Time: {}, Method: {}, Message: {}", new Date(), methodName, sw.toString());
	}

	@Override
	public void warn(final String className, final String methodName, final String message) {
		 logger = LoggerFactory.getLogger(className);
         logger.warn("Exceute Time: {}, Method: {}, Message: {}", new Date(), methodName, message);
	}

	@Override
	public void warn(final String className, final String methodName, final Exception exception) {
		logger = LoggerFactory.getLogger(className);
		StringWriter sw = setPrintStackTrace(exception);
		logger.warn("Exceute Time: {}, Method: {}, Message: {}", new Date(), methodName,sw.toString());
	}
	
	private StringWriter setPrintStackTrace(final Exception ex) {
        StringWriter sw = new StringWriter();
        ex.printStackTrace(new PrintWriter(sw, true));
        return sw;
    }
	/* (non-Javadoc)
	 * @see com.jeng.framework.core.log.Logger#info(java.lang.String)
	 */
	@Override
	public void info(final String message) {
		logger.info(message);
	}
	@Override
	public void info(final String message,final Throwable throwable) {
		logger.info(message,throwable);
	}
	/* (non-Javadoc)
	 * @see com.jeng.framework.core.log.Logger#debug(java.lang.String)
	 */
	@Override
	public void debug(final String message) {
		logger.debug(message);
	}
	@Override
	public boolean isDebugEnabled() {
		return logger.isDebugEnabled();
	}
	@Override
	public void error(final String message) {
		logger.error(message);
	}
	@Override
	public void error(final String message,final Throwable throwable) {
		logger.error(message,throwable);
	}
	@Override
	public void warn(final String message) {
		logger.error(message);
	}
}
