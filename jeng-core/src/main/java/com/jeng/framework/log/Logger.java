package com.jeng.framework.log;

/**
 * 
 * @Description: 	logger interface 
 * 
 * @author 			XinZheng Tan 
 * @date 			Oct 20, 2013 11:31:35 PM 
 *
 */
public interface Logger {
	
	/**
	 * 
	 * @return
	 */
	public boolean isDebugEnabled();
	
	/**
	 * print info level log only message
	 * @param message
	 * void
	 */
	public void info(final String message);
	
	
	/**
	 * <p>print info level log message
	 * @param message void
	 */
	public void info(final String message, final Throwable throwable);
	/**
	 * print info level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 */
	public void info(final Class<?> clazzClass, final String methodName, final String message);
	
	/**
	 * print info level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param exception     
	 * @return: 		void     
	 * @throws:
	 */
	public void info(final Class<?> clazzClass, final String methodName, final Exception exception);
	
	/**
	 * print info level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void info(final String className, final String methodName, final String message);
	
	/**
	 * 
	 * print info level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void info(final String className, final String methodName, final Exception message);
	
	/**
	 * <p>print error level log message
	 * @param message void
	 */
	public void error(final String message);
	
	/**
	 * <p>print error level log message
	 * @param message void
	 */
	public void error(final String message, final Throwable throwable);
	/**
	 * 
	 * print error level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void error(final Class<?> clazzClass, final String methodName, final String message);
	
	/**
	 * print error level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param exception     
	 * @return: 		void     
	 * @throws:
	 */
	public void error(final Class<?> clazzClass, final String methodName, final Exception exception);
	
	/**
	 * print error level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void error(final String className, final String methodName, final String message);
	
	/**
	 * print error level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void error(final String className, final String methodName, final Exception message);
	
	/**
	 * print debug level log only message
	 * @param message
	 * void
	 */
	public void debug(final String message);
	/**
	 * print debug level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void debug(final Class<?> clazzClass, final String methodName, final String message);
	
	/**
	 * print debug level log 
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param exception     
	 * @return: 		void     
	 * @throws:
	 */
	public void debug(final Class<?> clazzClass, final String methodName, final Exception exception);
	
	/**
	 * print debug level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void debug(final String className, final String methodName, final String message);
	
	/**
	 * print debug level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void debug(final String className, final String methodName, final Exception message);
	
	/**
	 * <p>print warn level log message
	 * @param message void
	 */
	public void warn(final String message);
	
	/**
	 * print warn level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void warn(final Class<?> clazzClass, final String methodName, final String message);
	
	/**
	 * print warn level log
	 * 
	 * @param: 			@param clazzClass
	 * @param: 			@param methodName
	 * @param: 			@param exception     
	 * @return: 		void     
	 * @throws:
	 */
	public void warn(final Class<?> clazzClass, final String methodName, final Exception exception);
	
	/**
	 * print warn level log
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void warn(final String className, final String methodName, final String message);
	
	/**
	 * print warn level log 
	 * 
	 * @param: 			@param className
	 * @param: 			@param methodName
	 * @param: 			@param message     
	 * @return: 		void     
	 * @throws:
	 */
	public void warn(final String className, final String methodName, final Exception message);
	
}
