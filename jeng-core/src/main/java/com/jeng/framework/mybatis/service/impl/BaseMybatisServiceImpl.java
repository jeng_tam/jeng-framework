package com.jeng.framework.mybatis.service.impl;

import com.jeng.framework.log.Logger;
import com.jeng.framework.log.LoggerFactory;
import com.jeng.framework.mybatis.dao.MybatisDao;
import com.jeng.framework.mybatis.service.BaseMybatisService;
import com.jeng.framework.service.impl.BaseServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.transaction.annotation.Transactional;

/*@Service
@Transactional*/
public class BaseMybatisServiceImpl extends BaseServiceImpl implements BaseMybatisService {

	public Logger log = LoggerFactory.getLogger(BaseMybatisServiceImpl.class);
	
	@Autowired( required = false)
	@Qualifier( value="mybatisDao")
	public MybatisDao mybatisDao;

}
