package com.jeng.framework.mybatis.model;

import com.jeng.framework.model.BaseModel;
import com.jeng.framework.mybatis.page.Page;

/**
 * Created by jengt_000 on 2014/12/23.
 */
public class BaseMybatisExample extends BaseModel {

    public Page pageInfo;

    public Page getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(Page pageInfo) {
        this.pageInfo = pageInfo;
    }
}
