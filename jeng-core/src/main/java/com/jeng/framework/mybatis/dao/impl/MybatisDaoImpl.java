package com.jeng.framework.mybatis.dao.impl;

import java.beans.Introspector;
import java.io.Serializable;
import java.util.*;

import com.jeng.framework.model.BaseModel;
import com.jeng.framework.mybatis.model.BaseMybatisExample;
import com.jeng.framework.mybatis.model.BaseMybatisModel;
import com.jeng.framework.mybatis.dao.MybatisDao;
import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.framework.mybatis.utils.ModelUtils;
import com.jeng.framework.mybatis.page.Page;
import com.jeng.framework.mybatis.page.PageInterceptor;
import com.jeng.framework.support.SpringContextUtil;
import com.jeng.framework.utils.AssertExt;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.mybatis.spring.support.SqlSessionDaoSupport;
import org.springframework.util.Assert;

public class MybatisDaoImpl extends SqlSessionDaoSupport implements MybatisDao {

    private static final String SQL_MAPPER_BASE_NAMESPACE = "com.jeng.framework.mybatis.mapper.xml.sql_mapper_base.";

    @Override
    public SqlSession getSqlSessionTemplate() {
        return getSqlSession();
    }

    @Override
    public Integer getLastPk() {
        return this.getSqlSessionTemplate().selectOne(SQL_MAPPER_BASE_NAMESPACE + "SELECT_INCREMENT_PK");
    }

    @Override
    public Date getSysdate() {
        return this.getSqlSessionTemplate().selectOne(SQL_MAPPER_BASE_NAMESPACE + "SELECT_SYSDATE");
    }

    @Override
    public <MODEL extends BaseMybatisModel> int countByModel(MODEL model) {
        AssertExt.isNullParameter(model);
        return getMybatisMapper(model.getClass()).countByModel(model);
    }

    @Override
    public <EXAMPLE extends BaseMybatisExample> int countByExample(EXAMPLE paramExample) {
        AssertExt.isNullParameter(paramExample);
        return getMybatisMapperByExample(paramExample.getClass()).countByExample(paramExample);
    }

    @Override
    public <MODEL extends BaseMybatisModel> boolean existByPrimaryKey(@Param("modelClass") Class<MODEL> paramClass, @Param("primaryKey") Serializable paramSerializable) {
        AssertExt.isNullParameter(paramSerializable);
        int rowCount = getMybatisMapper(paramClass).existByPrimaryKey(paramClass,paramSerializable);
        return rowCount == 1 ? true : false;
    }

    @Override
    public <MODEL extends BaseMybatisModel> boolean existByModel(MODEL model) {
        int resultCount = countByModel(model);
        return resultCount > 0 ? true : false;
    }

    @Override
    public <MODEL extends BaseMybatisExample> boolean existByExample(MODEL paramExample) {
        int resultCount = countByExample(paramExample);
        return resultCount > 0 ? true : false;
    }

    @Override
    public <MODEL extends BaseMybatisModel> int deleteByPrimaryKey(@Param("modelClass") Class<MODEL> paramClass, @Param("primaryKey") Serializable primaryKey) {
        AssertExt.isNullParameter(primaryKey);
        return getMybatisMapper(paramClass).deleteByPrimaryKey(paramClass, primaryKey);
    }

    @Override
    public <MODEL extends BaseMybatisModel> int delete(MODEL model) {
        AssertExt.isNullParameter(model);
        return getMybatisMapper(model.getClass()).delete(model);
    }

    @Override
    public <MODEL extends BaseMybatisModel> int deleteAllByPrimaryKey(Class<MODEL> paramClass, Collection<? extends Serializable> ids) {
        int row = 0;
        for (Serializable id : ids){
            deleteByPrimaryKey(paramClass,id);
            row++;
        }
        return row;
    }

    @Override
    public <MODEL extends BaseMybatisModel> int deleteAllByModel(Collection<MODEL> models) {
        int row = 0;
        for (Iterator iterator = models.iterator(); iterator.hasNext(); ) {
            BaseMybatisModel model = (BaseMybatisModel)iterator.next();
            delete(model);
            row++;
        }
        return row;
    }

    @Override
    public <EXAMPLE extends BaseMybatisExample> int deleteByExample(EXAMPLE paramExample) {
        AssertExt.isNullParameter(paramExample);
        return getMybatisMapperByExample(paramExample.getClass()).deleteByExample(paramExample);
    }

    @Override
    public <MODEL extends BaseMybatisModel> int insert(MODEL model) {
        AssertExt.isNullParameter(model);
        return getMybatisMapper(model.getClass()).insert(model);
    }

    @Override
    public <MODEL extends BaseMybatisModel> MODEL insertByModel(MODEL model) {
        int rowCount = insert(model);

        AssertExt.isInvalidResult(rowCount != 1, "Expected 1 lines affected result to be returned by insertByModel(), but found: " + rowCount + " line affected");

        Serializable primaryKey = ModelUtils.getPrimaryKeyValue(model);
        if (null == primaryKey) {
            primaryKey = getLastPk();
        }
        return (MODEL) selectByPrimaryKey(model.getClass(), primaryKey);
    }

    @Override
    public <MODEL extends BaseMybatisModel> MODEL selectByPrimaryKey(@Param("modelClass") Class<MODEL> paramClass, @Param("primaryKey") Serializable primaryKey) {
        AssertExt.isNullParameter(primaryKey);
        return getMybatisMapper(paramClass).selectByPrimaryKey(paramClass,primaryKey);
    }

    @Override
    public <MODEL extends BaseMybatisModel> List<MODEL> selectByModel(MODEL model) {
        AssertExt.isNullParameter(model);
        return getMybatisMapper(model.getClass()).selectByModel(model);
    }

    @Override
    public <MODEL extends BaseMybatisModel> MODEL selectOneByModel(MODEL model) {
        List<MODEL> resultList = selectByModel(model);

        AssertExt.isInvalidResult(1 < resultList.size(), "Expected one result (or null) to be returned by selectOneByModel(), but found: " + resultList.size());

        return (MODEL) AssertExt.isTrueLoose(1 == resultList.size(), resultList.get(0));
    }

    @Override
    public <MODEL extends BaseMybatisModel, EXAMPLE extends BaseMybatisExample> List<MODEL> selectByExample(EXAMPLE paramExample) {
        AssertExt.isNullParameter(paramExample);
        return getMybatisMapperByExample(paramExample.getClass()).selectByExample(paramExample);
    }

    @Override
    public <MODEL extends BaseMybatisModel, EXAMPLE extends BaseMybatisExample> MODEL selectOneByExample(EXAMPLE paramExample) {
        List<MODEL> resultList = selectByExample(paramExample);

        AssertExt.isInvalidResult(1 < resultList.size(), "Expected one result (or null) to be returned by selectOneByExample(), but found: " + resultList.size());
        if(resultList.size() == 0){
            return null;
        }
        return (MODEL) AssertExt.isTrueLoose(1 == resultList.size(), resultList.get(0));
    }

    @Override
    public <MODEL extends BaseMybatisModel> Page<MODEL> selectPageByModel(MODEL model) {
        AssertExt.isInvalidParameter(null == model, "Parameter model is require and can't support null object to be selectPageByModel(), but found: " + model);
        AssertExt.isInvalidParameter(null == model.getPageInfo(), "Parameter pageInfo is require and can't support null object to be selectPageByModel(), but found parameter pageInfo: " + model.getPageInfo());
        AssertExt.isInvalidParameter(0 >= model.getPageInfo().getPageSize() || 0 >= model.getPageInfo().getPageNum(),
                "Parameter pageInfo of pageSize and pageNum value must greater than 0 to be selectPageByModel()," +
                        " but found parameter pageInfo of pageSize: " + model.getPageInfo().getPageSize() + ", pageNum: " + model.getPageInfo().getPageNum());
        PageInterceptor.startPage(model.getPageInfo().getPageNum(), model.getPageInfo().getPageSize());
        selectByModel(model);
        return PageInterceptor.endPage();
    }

    @Override
    public <MODEL extends BaseMybatisModel, EXAMPLE extends BaseMybatisExample> Page<MODEL> selectPageByExample(EXAMPLE example) {
        AssertExt.isInvalidParameter(null == example,
                "Parameter model is require and can't support null object to be selectPageByModel(), but found: " + example);
        AssertExt.isInvalidParameter(null == example.getPageInfo(),
                "Parameter pageInfo is require and can't support null object to be selectPageByModel(), but found parameter pageInfo: " + example.getPageInfo());
        AssertExt.isInvalidParameter(0 >= example.getPageInfo().getPageSize() || 0 >= example.getPageInfo().getPageNum(),
                "Parameter pageInfo of pageSize and pageNum value must greater than 0 to be selectPageByModel()," +
                        " but found parameter pageInfo of pageSize: " + example.getPageInfo().getPageSize() + ", pageNum: " + example.getPageInfo().getPageNum());
        PageInterceptor.startPage(example.getPageInfo().getPageNum(), example.getPageInfo().getPageSize());
        selectByExample(example);
        return PageInterceptor.endPage();
    }

    @Override
    public <MODEL extends BaseMybatisModel> int update(MODEL model) {
        AssertExt.isNullParameter(model);
        Serializable primaryKey = ModelUtils.getPrimaryKeyValue(model);
        boolean isExist = existByPrimaryKey(model.getClass(), primaryKey);
        AssertExt.isInvalidResult(!isExist, "Row was deleted by another transaction (or unsaved-value mapping was incorrect)");
        int updateCount = getMybatisMapper(model.getClass()).update(model);
        AssertExt.isInvalidResult(1 < updateCount,  "Row was updated by another transaction (or unsaved-value mapping was incorrect)");
        return (int) AssertExt.isTrueLoose(1 == updateCount, updateCount);
    }

    @Override
    public <MODEL extends BaseMybatisModel> MODEL updateByModel(MODEL model) {
        update(model);
        Integer primaryKey = (Integer) ModelUtils.getPrimaryKeyValue(model);
        if (primaryKey == null || primaryKey == 0) {
            primaryKey = getLastPk();
        }
        return (MODEL) selectByPrimaryKey(model.getClass(),primaryKey);
    }

    @Override
    public <MODEL extends BaseMybatisModel, EXAMPLE extends BaseMybatisExample> int updateByExampleSelective(@Param("record") MODEL model, @Param("example") EXAMPLE paramExample) {
        AssertExt.isNullParameter(model);
        AssertExt.isNullParameter(paramExample);
        return getMybatisMapperByExample(paramExample.getClass()).updateByExampleSelective(model, paramExample);
    }

    @Override
    public <MODEL extends BaseMybatisModel> int save(MODEL model) {
        AssertExt.isNullParameter(model);
        if (BaseModel.ROW_STATE_DELETED.equals(model.getRowState())) {
            return delete(model);
        }
        Serializable idValue = ModelUtils.getPrimaryKeyValue(model);
        boolean isExists = existByPrimaryKey(model.getClass(), idValue);
        if (isExists) {
            return update(model);
        }
        return insert(model);
    }

    @Override
    public <MODEL extends BaseMybatisModel> MODEL saveByModel(MODEL model) {
        AssertExt.isNullParameter(model);
        if (BaseModel.ROW_STATE_DELETED.equals(model.getRowState())) {
            int row = delete(model);
            AssertExt.isInvalidResult(1 != row, "Expected 1 to be returned by saveByModel() of delete function, but found: " + row);
            return null;
        }
        Serializable idValue = ModelUtils.getPrimaryKeyValue(model);
        boolean isExists = existByPrimaryKey(model.getClass(), idValue);
        if (isExists) {
            return updateByModel(model);
        }
        return insertByModel(model);
    }

    @Override
    public <MODEL extends BaseMybatisModel> List<MODEL> saveAllByModel(Collection<MODEL> models) {
        AssertExt.isNullParameter(models);
        List<MODEL> modelsToDelete = new ArrayList<MODEL>();
        List<MODEL> modelsToMerge = new ArrayList<MODEL>();
        List<MODEL> result = new ArrayList<MODEL>();
        for (Iterator iterator = models.iterator(); iterator.hasNext(); ) {
            BaseMybatisModel model = (BaseMybatisModel)iterator.next();
            if (BaseModel.ROW_STATE_DELETED.equals(model.getRowState())){
                modelsToDelete.add((MODEL)model);
            }else {
                modelsToMerge.add((MODEL)model);
            }
        }
        deleteAllByModel(modelsToDelete);
        for (BaseMybatisModel model : modelsToMerge) {
            Serializable idValue = ModelUtils.getPrimaryKeyValue(model);
            boolean isExists = existByPrimaryKey(model.getClass(), idValue);
            if (isExists) { //存在此记录则进行更新操作
                result.add((MODEL) updateByModel(model));
            } else { //若不存在则进行新增操作
                result.add((MODEL) insertByModel(model));
            }
        }
        return result;
    }

    /**
     * 根据model类获取相应mapper
     * @param modelClass
     * @param <MODEL>
     * @return
     */
    private <MODEL extends BaseMybatisModel> MybatisMapper getMybatisMapper(Class<MODEL> modelClass) {
        String modelName = modelClass.getSimpleName();
        String mapperName = Introspector.decapitalize(modelName) + "Mapper";
        Object objMapper = SpringContextUtil.getBean(mapperName);
        Assert.notNull(objMapper, "mapper " + mapperName + "extends MybatisMapper not exist");
        Assert.isInstanceOf(MybatisMapper.class, objMapper, "mapper " + mapperName + "not extends MybatisMapper");
        return (MybatisMapper) objMapper;
    }

    /**
     * 根据example类获取相应mapper
     * @param exampleClass
     * @param <Model>
     * @return
     */
    private <MODEL extends BaseMybatisExample> MybatisMapper getMybatisMapperByExample(Class<MODEL> exampleClass) {
        String exampleClassSimpleName = exampleClass.getSimpleName();
        int nameLength = exampleClassSimpleName.length();
        String modelName = exampleClassSimpleName.substring(0, nameLength - 7);
        String mapperName = Introspector.decapitalize(modelName) + "Mapper";
        Object objMapper = SpringContextUtil.getBean(mapperName);
        Assert.notNull(objMapper, "mapper " + mapperName + "extends MybatisMapper not exist");
        Assert.isInstanceOf(MybatisMapper.class, objMapper, "mapper " + mapperName + "not extends MybatisMapper");
        return (MybatisMapper) objMapper;
    }

}
