package com.jeng.framework.model;

import com.jeng.framework.core.BasePo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jengt_000 on 2014/12/28.
 */
public class BaseModel extends BasePo implements Serializable{

    //对应数据库数据版本字段，请勿随意修改
    public static final String ROW_RECORD_VERSION_FIELD = "RECORD_VERSION";

    public static final String ROW_STATUS_INITIAL = "Initial";
    public static final String ROW_STATE_ADDED = "Added";
    public static final String ROW_STATE_DELETED = "Deleted";
    public static final String ROW_STATE_MODIFIED = "Modified";
    private String rowState;
    private List<String> validFields = new ArrayList();

    public String getRowState() {
        return this.rowState;
    }

    public void setRowState(String rowState) {
        this.rowState = rowState;
    }

    public boolean isInitilRow() {
        return rowState != null && ROW_STATUS_INITIAL.equals(rowState);
    }

    public boolean isAddRow() {
        return rowState != null && ROW_STATE_ADDED.equals(rowState);
    }

    public boolean isModifyRow() {
        return rowState != null && ROW_STATE_MODIFIED.equals(rowState);
    }

    public boolean isDeletedRow() {
        return rowState != null && ROW_STATE_DELETED.equals(rowState);
    }

    protected void addValidField(String fieldName) {
        if (!this.validFields.contains(fieldName)){
            this.validFields.add(fieldName);
        }
    }

    protected void removeValidField(String fieldName) {
        if (this.validFields.contains(fieldName)){
            this.validFields.remove(fieldName);
        }
    }

    public List<String> validFields()
    {
        return this.validFields;
    }
}
