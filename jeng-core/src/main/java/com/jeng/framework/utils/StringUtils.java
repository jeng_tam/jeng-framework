package com.jeng.framework.utils;

import java.util.UUID;

/**
 * Created by jengt_000 on 2014/12/23.
 */
public class StringUtils extends BaseUtils {

    /**
     * 生成32位UUID
     *
     * @return
     */
    public static String getUUID() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        return str.replaceAll("-", "");
    }

}
