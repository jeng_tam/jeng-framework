package com.jeng.framework.service.impl;

import com.jeng.framework.log.Logger;
import com.jeng.framework.log.LoggerFactory;
import com.jeng.framework.service.BaseService;

public class BaseServiceImpl implements BaseService {

	public Logger log = LoggerFactory.getLogger(BaseServiceImpl.class);
}
