package com.jeng.test.mybatis.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.test.mybatis.model.TEmployeeExample;
import com.jeng.test.mybatis.model.TEmployee;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface TEmployeeMapper extends MybatisMapper {
    int countByExample(TEmployeeExample example);

    int deleteByExample(TEmployeeExample example);

    List<TEmployee> selectByExample(TEmployeeExample example);

    int updateByExampleSelective(@Param("record") TEmployee record, @Param("example") TEmployeeExample example);
}