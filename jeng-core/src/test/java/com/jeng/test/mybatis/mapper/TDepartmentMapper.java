package com.jeng.test.mybatis.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.test.mybatis.model.TDepartment;
import com.jeng.test.mybatis.model.TDepartmentExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface TDepartmentMapper extends MybatisMapper {
    int countByExample(TDepartmentExample example);

    int deleteByExample(TDepartmentExample example);

    List<TDepartment> selectByExample(TDepartmentExample example);

    int updateByExampleSelective(@Param("record") TDepartment record, @Param("example") TDepartmentExample example);
}