package com.jeng.framework.memcached;

import com.jeng.test.BaseSpringTest;
import com.whalin.MemCached.MemCachedClient;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class MemCachedUtilTest extends BaseSpringTest{

    @Autowired
    MemCachedUtil memCachedUtil;

    @Test
    public void testAdd() throws Exception {
        String key = "username";
        String value = "谭新政";
        memCachedUtil.add(key, value);
        Assert.assertEquals(value, memCachedUtil.get(key));
    }

    @Test
    public void testAdd1() throws Exception {

    }

    @Test
    public void testPut() throws Exception {

    }

    @Test
    public void testPut1() throws Exception {

    }

    @Test
    public void testReplace() throws Exception {

    }

    @Test
    public void testReplace1() throws Exception {

    }

    @Test
    public void testGet() throws Exception {

    }
}