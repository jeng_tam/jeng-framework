package com.jeng.framework.utils;

import org.junit.Assert;
import org.junit.Test;

public class StringUtilsTest {

    @Test
    public void testGetUUID() throws Exception {
        String uuid = StringUtils.getUUID();
        Assert.assertTrue(uuid.length() == 32 && uuid.matches("^[a-z0-9]+$"));
    }
}