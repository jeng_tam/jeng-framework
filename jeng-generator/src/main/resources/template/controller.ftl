package com.udfexpress.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ceaser.framework.web.controller.BaseController;
import com.ceaser.framework.exception.BusinessException;
import com.ceaser.dao.${table.name}Dao;
import com.ceaser.entity.${table.name};

@RestController
public class ${table.name}Controller extends BaseController {
	
	@Autowired private ${table.name}Dao ${table.privateName}Dao;
	
	@RequestMapping("/${table.privateName}/select${table.name}ById")
    public ${table.name} select${table.name}ById(
            @RequestParam(value="${table.privateName}Id", required=false, defaultValue="0") int ${table.privateName}Id)
             throws BusinessException  {
        return ${table.privateName}Dao.select${table.name}ById(${table.privateName}Id) ;
    }
	
	@RequestMapping("/${table.privateName}/selectAll${table.name}")
    public List<${table.name}> selectAll${table.name}() throws BusinessException {
        return ${table.privateName}Dao.selectAll${table.name}() ;
    }	
	
	@RequestMapping(value={"/${table.privateName}/update${table.name}"})
    public ${table.name} update${table.name}(@RequestBody ${table.name} ${table.privateName}) 
    		 throws BusinessException {
        return ${table.privateName}Dao.update${table.name}(${table.privateName}) ;
    }		

	@RequestMapping(value={"/${table.privateName}/insert${table.name}"})
    public ${table.name} insert${table.name}(@RequestBody ${table.name} ${table.privateName}) 
    		 throws BusinessException {
        return ${table.privateName}Dao.insert${table.name}(${table.privateName}) ;
	}

	@RequestMapping(value={"/${table.privateName}/delete${table.name}"})
    public int delete${table.name}(@RequestParam(value="${table.privateName}Id", required=false, defaultValue="0") int ${table.privateName}Id) 
    		 throws BusinessException {
       return ${table.privateName}Dao.delete${table.name}(${table.privateName}Id) ;        
        
    }

	@RequestMapping(value={"/${table.privateName}/save${table.name}"})
    public ${table.name} save${table.name}(@RequestBody ${table.name} ${table.privateName}) 
    		 throws BusinessException {
        return ${table.privateName}Dao.save${table.name}(${table.privateName}) ;
	}	
}
