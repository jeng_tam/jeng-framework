package com.udfexpress.entity;

import java.util.Date;
import java.math.BigDecimal;
import org.springframework.stereotype.Component;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import com.udfexpress.framework.core.BasePo;

@Component
@JsonIgnoreProperties(ignoreUnknown=true)
public class ${table.name} extends BasePo {

	private static final long serialVersionUID = 1L;
	
    <#list table.fields as field>
    private ${field.javaType} ${field.privatePropertyName};
	</#list>
    
    <#list table.fields as field>
    public ${field.javaType} get${field.propertyName}() {
        return ${field.privatePropertyName};
    }
        
    public void set${field.propertyName}(${field.javaType} ${field.privatePropertyName}) {
        this.${field.privatePropertyName} = ${field.privatePropertyName};
    }
        
	</#list>
}