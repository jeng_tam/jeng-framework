package com.caesar.generator.freemarker.table;

import com.caesar.generator.freemarker.utils.Util;

import java.util.ArrayList;
import java.util.List;


public class Table {
	private String privateName;
	private String rawName;
	private String name;
	private String keyField;
	private String rawKeyField; //only one key support.
	private String privateKeyField;
	private List<Field> fields;

	public List<Field> getFields() {
		if(fields==null){
			fields = new ArrayList<Field>();
		}
		return fields;
	}

	public void setFields(List<Field> fields) {
		this.fields = fields;
	}

	public String getPrivateName() {
		return privateName;
	}

	public void setPrivateName(String name) {
		this.privateName = name;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRawName() {
		return rawName;
	}

	public void setRawName(String rawName) {
		this.rawName = rawName;
		this.setPrivateName(Util.underScore2CamelCase(rawName.toString()));
		this.setName(Util.upcaseFirstLetter(Util.underScore2CamelCase(rawName.toString())));
	}

	public String getRawKeyField() {
		return rawKeyField;
	}

	public void setRawKeyField(String rawKeyField) {
		this.rawKeyField = rawKeyField;
		this.setPrivateKeyField(Util.underScore2CamelCase(rawKeyField.toString()));
		this.setKeyField(Util.upcaseFirstLetter(Util.underScore2CamelCase(rawKeyField.toString())));
	}

	public String getKeyField() {
		return keyField;
	}

	public void setKeyField(String keyField) {
		this.keyField = keyField;
	}

	public String getPrivateKeyField() {
		return privateKeyField;
	}

	public void setPrivateKeyField(String privateKeyField) {
		this.privateKeyField = privateKeyField;
	}

}
