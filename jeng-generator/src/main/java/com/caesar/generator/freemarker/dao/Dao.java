package com.caesar.generator.freemarker.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.caesar.generator.freemarker.table.Field;
import com.caesar.generator.freemarker.table.Table;
import com.caesar.generator.freemarker.utils.Util;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;

public class Dao {

	public static Table getTableObj(String tableName) throws ConfigurationException {
		PropertiesConfiguration config;  
		config = new PropertiesConfiguration("properties/config.properties");
		
		String url=config.getString("url");
		String username=config.getString("username");
		String password=config.getString("password");
		String driverClassName=config.getString("driverClassName");
		String tableDesc=config.getString("tableDesc");

		
		Connection con = null;
		Statement smt = null;
		Table table = new Table();
		table.setRawName(tableName);
		
		try {
			Class.forName(driverClassName);
			con = DriverManager.getConnection(url, username, password);
			smt = con.createStatement();
			ResultSet res = smt.executeQuery(tableDesc + " " + tableName);

			while (res.next()) {
				Field fld = new Field();
				String fieldName=res.getString("Field");
				if(res.getString("Key").equals("PRI")){//only one key field /mysql supported.
					table.setRawKeyField(fieldName);
				}
					
				String privatePropertyName = Util.underScore2CamelCase(fieldName);
				fld.setColumnName(fieldName);
				fld.setPrivatePropertyName(privatePropertyName);
				fld.setType(res.getString("Type"));
				
				table.getFields().add(fld);
			}
			res.close(); 

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		}
		
		return table;
	}
	
	public static List<Object> getAllTableObj() throws ConfigurationException {
		PropertiesConfiguration config;  
		config = new PropertiesConfiguration("properties/config.properties");
		
		String url=config.getString("url");
		String username=config.getString("username");
		String password=config.getString("password");
		String driverClassName=config.getString("driverClassName");
		String tableDesc=config.getString("tableDesc");
		String tableSchema=config.getString("tableSchema");
		
		Connection con = null;
		Statement smt = null;
		List<Object> tables = new ArrayList<Object>();
		
		try {
			Class.forName(driverClassName);
			con = DriverManager.getConnection(url, username, password);
			smt = con.createStatement();
			ResultSet res = smt.executeQuery("SELECT table_name FROM information_schema.tables WHERE table_schema='"+tableSchema+"'");
			while (res.next()) {
				String fieldName=res.getString("table_name");
				if(!fieldName.equals("operation_events_afterhandle") && !fieldName.equals("operation_events_beforehandle")){
					tables.add(fieldName);
				}
			}
			res.close(); 

		} catch (Exception ex) {
			ex.printStackTrace(); 
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		}
		
		return tables;
	}
}
