package com.caesar.generator.freemarker.main;

import com.caesar.generator.freemarker.utils.Util;
import com.caesar.generator.freemarker.dao.Dao;
import com.caesar.generator.freemarker.table.Table;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.io.FileUtils;

import freemarker.template.*;

import java.util.*;
import java.io.*;

public class FreeMarker{

	public static void main(String[] args) throws Exception {
		codeGenBatch();
	}
	
	public static void codeGenBatch() throws ConfigurationException, IOException, TemplateException{
		PropertiesConfiguration config;  
		config = new PropertiesConfiguration("properties/config.properties");
		List<Object> tableNames=config.getList("tableName");
		List<Object> templateNames=config.getList("templateName"); 
		String outputDir=config.getString("outputDir");
		String daoDir=config.getString("daoDir");
		String infDir=config.getString("infDir");
		String sqlProviderDir=config.getString("sqlProviderDir");
		String controllerDir=config.getString("controllerDir");
		String entityDir=config.getString("entityDir");
		String templateDir=config.getString("templateDir");
		String isTableAll=config.getString("isTableAll");
		
		String ftl;
		String outExt;
		String[] template;
		
		if("true".equals(isTableAll)){
			tableNames = Dao.getAllTableObj();
		}
		
		for(Object tableName:tableNames){
			System.out.println(tableName);
			for(Object templateName:templateNames){
				template = config.getStringArray(templateName.toString());
				ftl=template[0];
				outExt=template[1];
				String outDir = "";
				if("controller.ftl".equals(ftl)){
					outDir = controllerDir;
				}else if("dao.ftl".equals(ftl)){
					outDir = daoDir; 
				}else if("intf.ftl".equals(ftl)){
					outDir = infDir;
				}else if("model.ftl".equals(ftl)){
					outDir = entityDir;
				}else if("sqlProvider.ftl".equals(ftl)){
					outDir = sqlProviderDir;
				}
				codeGen(tableName.toString(),ftl,templateDir, outDir +
						Util.upcaseFirstLetter(Util.underScore2CamelCase(tableName.toString())) + outExt);
			}
		}
	}
	
	public static void codeGen(String tableName,String templateName,String templateDir,String outputFile) throws ConfigurationException, IOException, TemplateException{
		Table table = Dao.getTableObj(tableName);
		Map<String, Object> root = new HashMap<String, Object>();
	    root.put("table", table);
	    
	    StringWriter out = new StringWriter();

	    Configuration cfg = new Configuration();
		cfg.setDirectoryForTemplateLoading(new File(templateDir));
	    cfg.setObjectWrapper( new DefaultObjectWrapper() );
	    Template temp = cfg.getTemplate( templateName);
	    temp.process( root, out );
	    String content = out.getBuffer().toString();
	    System.out.println( content );
	    File file = new File(outputFile);
	    FileUtils.writeStringToFile(file, content, "UTF-8");
	    out.flush();		
	}
}