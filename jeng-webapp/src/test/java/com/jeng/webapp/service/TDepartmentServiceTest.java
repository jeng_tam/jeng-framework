package com.jeng.webapp.service;

import com.jeng.framework.mybatis.dao.MybatisDao;
import com.jeng.webapp.model.TDepartment;
import com.jeng.webapp.test.BaseSpringTest;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TDepartmentServiceTest extends BaseSpringTest {

    @Autowired
    TDepartmentService tDepartmentService;
    @Autowired
    MybatisDao mybatisDao;

    @Test
    public void testUpdateByPrimaryKeySelective() throws Exception {
        TDepartment tDepartment = new TDepartment();
        tDepartment.settDepartmentId("888");
        tDepartment.setName("老西888");
        tDepartment.setRecordVersion(4);
        try {
            int i = tDepartmentService.updateByPrimaryKeySelective(tDepartment);
            System.out.println(i);
        } catch (Exception e){
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testSelectByModel() throws Exception {
        TDepartment tDepartment = new TDepartment();
        tDepartment.settDepartmentId("888");
        tDepartment.setName("老西888");
        try {
            List<TDepartment> list = mybatisDao.selectByModel(tDepartment);
            Assert.assertTrue(list.size() > 0);
        } catch (Exception e){
            Assert.fail(e.getMessage());
        }
    }

    @Before
    public void setUp() throws Exception {

    }

    @Test
    public void testCountByExample() throws Exception {

    }

    @Test
    public void testDeleteByExample() throws Exception {

    }

    @Test
    public void testDeleteByPrimaryKey() throws Exception {

    }

    @Test
    public void testInsertSelective() throws Exception {
        TDepartment tDepartment = new TDepartment();
        tDepartment.setName("测试数据");
        Integer i = tDepartmentService.insertSelective(tDepartment);
        Assert.assertTrue("新增失败",i==1);
    }

    @Test
    public void testSelectByExample() throws Exception {

    }

    @Test
    public void testSelectByPrimaryKey() throws Exception {

    }

    @Test
    public void testUpdateByExampleSelective() throws Exception {

    }

    @Test
    public void testUpdateByPrimaryKeySelective1() throws Exception {

    }
}