package com.jeng.webapp.service;

import com.jeng.webapp.test.BaseSpringTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class DemoServiceTest extends BaseSpringTest {

    @Autowired
    DemoService demoService;

    @Test
    public void testInsertTestTrancation() throws Exception {
        try{
            demoService.insertTestTrancation();
            Assert.fail("事务进行未回滚");
        }catch (RuntimeException e){
            Assert.assertTrue(true);
        }

    }
}