package com.jeng.webapp.test;

import com.jeng.webapp.model.TDepartment;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import javax.persistence.Column;
import java.beans.PropertyDescriptor;

/**
 * Created by jengt_000 on 2014/12/28.
 */
public class TestMain {

    public static void main(String[] args) {
        TDepartment tDepartment = new TDepartment();
        tDepartment.setName("长沙");
        tDepartment.settDepartmentId("333333333f");
        BeanWrapper beanWrapper = new BeanWrapperImpl(tDepartment);
        for (PropertyDescriptor propertyDescriptor : beanWrapper.getPropertyDescriptors()) {
            if (beanWrapper
                    .isReadableProperty(propertyDescriptor.getName())
                    && propertyDescriptor.getReadMethod()
                    .isAnnotationPresent(Column.class)) {
                Column column = propertyDescriptor.getReadMethod()
                        .getAnnotation(Column.class);
                String javaType = propertyDescriptor.getPropertyType()
                        .getSimpleName();
            }
        }
    }

}
