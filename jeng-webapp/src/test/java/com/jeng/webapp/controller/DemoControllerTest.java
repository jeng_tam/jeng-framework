package com.jeng.webapp.controller;

import com.jeng.webapp.test.BaseSpringTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


public class DemoControllerTest extends BaseSpringTest {

    String result = "{\"status\":0,\"result\":[{\"tDepartmentId\":\"333\",\"name\":\"发财\",\"description\":null,\"createByUser\":null,\"createDateTime\":null,\"createOffice\":null,\"createTimeZone\":null,\"recordVersion\":null,\"updateByUser\":null,\"updateDateTime\":null,\"updateOffice\":null,\"updateTimeZone\":null},{\"tDepartmentId\":\"444\",\"name\":\"???\",\"description\":null,\"createByUser\":null,\"createDateTime\":null,\"createOffice\":null,\"createTimeZone\":null,\"recordVersion\":null,\"updateByUser\":null,\"updateDateTime\":null,\"updateOffice\":null,\"updateTimeZone\":null},{\"tDepartmentId\":\"666\",\"name\":\"???\",\"description\":null,\"createByUser\":null,\"createDateTime\":null,\"createOffice\":null,\"createTimeZone\":null,\"recordVersion\":null,\"updateByUser\":null,\"updateDateTime\":null,\"updateOffice\":null,\"updateTimeZone\":null},{\"tDepartmentId\":\"777\",\"name\":\"???\",\"description\":null,\"createByUser\":null,\"createDateTime\":null,\"createOffice\":null,\"createTimeZone\":null,\"recordVersion\":null,\"updateByUser\":null,\"updateDateTime\":null,\"updateOffice\":null,\"updateTimeZone\":null},{\"tDepartmentId\":\"888\",\"name\":\"谭新政\",\"description\":null,\"createByUser\":null,\"createDateTime\":null,\"createOffice\":null,\"createTimeZone\":null,\"recordVersion\":null,\"updateByUser\":null,\"updateDateTime\":null,\"updateOffice\":null,\"updateTimeZone\":null}],\"pageInfo\":null,\"msg\":\"请求成功\",\"exceptionMsg\":null,\"notConvert\":false}";

    @Test
    public void testDemo() throws Exception {
        super.mockMvc.perform(get("/login.json").accept(MediaType.parseMediaType("application/json;charset=UTF-8")))
                    .andExpect(status().isOk())
                    .andExpect(content().contentType("application/json;charset=UTF-8"))
                    .andExpect(content().string(result));

    }
}