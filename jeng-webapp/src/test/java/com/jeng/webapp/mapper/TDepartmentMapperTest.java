package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.dao.MybatisDao;
import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.TDepartment;
import com.jeng.webapp.model.TDepartmentExample;
import com.jeng.webapp.test.BaseSpringTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class TDepartmentMapperTest  extends BaseSpringTest {


    @Autowired
    MybatisDao mybatisDao;

    MybatisMapper mybatisMapper;

    @Test
    public void testDeleteByPrimaryKey() throws Exception {
        List<TDepartment> tDepartments =  mybatisMapper.selectByExample(new TDepartmentExample());
        TDepartment tDepartment = tDepartments.get(0);
        int i = mybatisMapper.deleteByPrimaryKey(TDepartment.class,tDepartment.gettDepartmentId());
        System.out.println(i);
        Assert.assertTrue(i < 0);
    }

    @Test
    public void testInsert() throws Exception {

    }

    @Test
    public void testInsertSelective() throws Exception {

    }

    @Test
    public void testSelectByExample() throws Exception {

    }

    @Test
    public void testSelectByPrimaryKey() throws Exception {

    }

    @Test
    public void testUpdateByExampleSelective() throws Exception {
        System.out.println(mybatisDao.getSysdate());
    }

    @Test
    public void testUpdateByExample() throws Exception {
        TDepartmentExample tDepartmentExample = new TDepartmentExample();
        tDepartmentExample.createCriteria().andNameIsNotNull();
        TDepartment tDepartment = new TDepartment();
        tDepartment.setName("测试成功");
        tDepartment.setRecordVersion(43);
       /* int i = tDepartmentMapper.updateByExample(tDepartment,tDepartmentExample);
        System.out.println(i);*/
    }

    @Test
    public void testUpdateByPrimaryKeySelective() throws Exception {

    }

    @Test
    public void testUpdateByPrimaryKey() throws Exception {

    }
}