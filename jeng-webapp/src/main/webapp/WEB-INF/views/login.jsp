<%--
  Created by IntelliJ IDEA.
  User: jengt_000
  Date: 2015/1/18
  Time: 15:57
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<script type="application/javascript" src="${pageContext.request.contextPath}/resources/jquery.js"></script>
<body>
页面登录
<form action="${pageContext.request.contextPath}/j_spring_security_check" method="post">
  <input id="j_username" type="text" name="j_username">
  <input id="j_password" type="password" name="j_password">
  <button type="submit">提交</button>
</form>
<button id="ajaxLogin" type="button">Ajax登录</button>
<script>
  $("#ajaxLogin").click(function(){
      login();
  });
  function login(){
    var app = "${pageContext.request.contextPath}";
    var username = $("#j_username").val();
    var password = $("#j_password").val();
    $.post(app + "/j_spring_security_check",{
      j_username:username,
      j_password:password
    },function(data){
      console.log(data);
    })
  }

</script>
</body>
</html>
