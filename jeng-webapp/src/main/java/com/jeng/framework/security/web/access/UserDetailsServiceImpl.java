package com.jeng.framework.security.web.access;

import com.jeng.framework.mybatis.service.impl.BaseMybatisServiceImpl;
import com.jeng.webapp.model.CdUser;
import com.jeng.webapp.model.CdUserExample;
import com.jeng.webapp.model.CdUserPwd;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

/**
 * Created by jengt_000 on 2015/1/17.
 */
public class UserDetailsServiceImpl extends BaseMybatisServiceImpl implements UserDetailsService{

    @Autowired(required = false)
    AuthenticationService authenticationService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        CdUserExample cdUserExample = new CdUserExample();
        cdUserExample.createCriteria().andUserNameEqualTo(username);
        cdUserExample.or().andFullNameEqualTo(username);
        CdUser cdUser = mybatisDao.selectOneByExample(cdUserExample);
        if(null == cdUser){
            throw new UsernameNotFoundException(username + " not exist ");
        }
        CdUserPwd cdUserPwd = new CdUserPwd();
        cdUserPwd.setCdUserId(cdUser.getCdUserId());
        CdUserPwd result = mybatisDao.selectOneByModel(cdUserPwd);
        return new User(cdUser.getUserName(), result.getPwd(), authenticationService.getGrantedAuthority(cdUser.getUserName()));
    }
}
