package com.jeng.framework.security.web.authentication.ajax;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Spring Security3 Ajax登录认证失败处理实现类
 */
public class AjaxLoginAuthenticationFailureHandler extends AjaxLoginAuthenticationHandler implements AuthenticationFailureHandler {

    @Override
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        AuthenticationException exception) throws IOException, ServletException {
        super.onAuthenticationFailure(httpServletRequest, httpServletResponse, exception);
    }
}
