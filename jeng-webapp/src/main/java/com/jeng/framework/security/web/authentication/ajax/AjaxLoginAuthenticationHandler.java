package com.jeng.framework.security.web.authentication.ajax;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jeng.framework.web.vo.JsonVo;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by jengt_000 on 2015/1/23.
 */
public class AjaxLoginAuthenticationHandler {

    /**
     * ajax登录认证成功处理方法
     * @param httpServletRequest
     * @param httpServletResponse
     * @param authentication
     * @throws IOException
     * @throws ServletException
     */
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        JsonVo jsonData = new JsonVo(1);
        jsonData.setMessage("Login success, " + authentication.getName() + " welcome!");
        onAuthenticationHandle(jsonData, httpServletRequest, httpServletResponse);
    }

    /**
     * ajax登录认证失败处理方法
     * @param httpServletRequest
     * @param httpServletResponse
     * @param exception
     * @throws IOException
     * @throws ServletException
     */
    public void onAuthenticationFailure(HttpServletRequest httpServletRequest,
                                        HttpServletResponse httpServletResponse,
                                        AuthenticationException exception) throws IOException, ServletException {
        JsonVo jsonData = new JsonVo(-1);
        jsonData.setMessage(exception.getMessage());
        jsonData.setExceptionMsg(exception.getLocalizedMessage());
        onAuthenticationHandle(jsonData, httpServletRequest, httpServletResponse);
    }

    /**
     * ajax login return json data type handle main function
     * @param result
     * @param httpServletRequest
     * @param httpServletResponse
     * @throws IOException
     * @throws ServletException
     */
    private void onAuthenticationHandle(Object result, HttpServletRequest httpServletRequest,
                                          HttpServletResponse httpServletResponse) throws IOException, ServletException{
        ObjectMapper objectMapper = new ObjectMapper();
        httpServletResponse.setHeader("Content-Type", "application/json;charset=UTF-8");
        JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(httpServletResponse.getOutputStream(),
                JsonEncoding.UTF8);
        try {
            objectMapper.writeValue(jsonGenerator, result);
        } catch (JsonProcessingException ex) {
            throw new HttpMessageNotWritableException("Could not write JSON: " + ex.getMessage(), ex);
        }
    }

}
