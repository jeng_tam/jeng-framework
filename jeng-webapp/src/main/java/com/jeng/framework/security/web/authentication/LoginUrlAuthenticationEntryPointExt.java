package com.jeng.framework.security.web.authentication;

import com.fasterxml.jackson.core.JsonEncoding;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jeng.framework.log.Logger;
import com.jeng.framework.log.LoggerFactory;
import com.jeng.framework.web.vo.JsonVo;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 登录URL认证切面继承类（未完成）
 * 1.重写Spring Security3的默认页面登录方式
 * 2.添加Ajax登录
 * 3.支持普通页面登录及Ajax登录
 */
public class LoginUrlAuthenticationEntryPointExt extends LoginUrlAuthenticationEntryPoint {

    private Logger logger = LoggerFactory.getLogger(getClass());

    public LoginUrlAuthenticationEntryPointExt(String loginFormUrl) {
        super(loginFormUrl);
    }

    private boolean forceHttps = false;
    private boolean useForward = false;
    private final RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException, ServletException {
        String redirectUrl = null;
        String requestURI = request.getRequestURI();
        if (logger.isDebugEnabled()) {
            logger.debug("URL: " + requestURI);
        }
        if(-1 == requestURI.indexOf("ajax")){//普通页面登录
            if (this.useForward) {
                if (this.forceHttps && "http".equals(request.getScheme())) {
                    redirectUrl = this.buildHttpsRedirectUrlForRequest(request);
                }
                if (redirectUrl == null) {
                    String loginForm = this.determineUrlToUseForThisRequest(request, response, authException);
                    if (logger.isDebugEnabled()) {
                        logger.debug("Server side forward to: " + loginForm);
                    }
                    RequestDispatcher dispatcher = request.getRequestDispatcher(loginForm);
                    dispatcher.forward(request, response);
                    return;
                }
            } else {
                redirectUrl = this.buildRedirectUrlToLoginPage(request, response, authException);
            }
            this.redirectStrategy.sendRedirect(request, response, redirectUrl);
        }else{
            // ajax请求，返回json，替代redirect到login page
            if (logger.isDebugEnabled()) {
                logger.debug("ajax request or post");
            }
            ObjectMapper objectMapper = new ObjectMapper();
            response.setHeader("Content-Type", "application/json;charset=UTF-8");
            JsonGenerator jsonGenerator = objectMapper.getFactory().createGenerator(response.getOutputStream(),
                    JsonEncoding.UTF8);
            try {
                JsonVo jsonData = new JsonVo(2);
                objectMapper.writeValue(jsonGenerator, jsonData);
            } catch (JsonProcessingException ex) {
                throw new HttpMessageNotWritableException("Could not write JSON: " + ex.getMessage(), ex);
            }
        }
    }
}
