package com.jeng.framework.security.web.access;

import com.jeng.framework.mybatis.service.impl.BaseMybatisServiceImpl;
import com.jeng.webapp.model.CdResource;
import com.jeng.webapp.model.CdRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.util.AntPathMatcher;

import java.util.*;

/**
 * Created by jengt_000 on 2015/1/17.
 */
public class InvocationSecurityMetadataSourceService extends BaseMybatisServiceImpl implements FilterInvocationSecurityMetadataSource {

    private static Map<String, Collection<ConfigAttribute>> resourceMap = null;

    private AntPathMatcher antPathMatcher = new AntPathMatcher();

    @Autowired(required = false)
    AuthenticationService authenticationService;

    /**
     * 加载角色权限
     * 注：1.数据库权限资源必须存在url=/**的资源，对所有请求进行拦截，否则spring security不会拦截任何请求
     *     2.数据库权限资源的Url必须以‘/’符号开头
     */
    public void loadResourceDefine(){
        this.resourceMap = loadResourceDefineMain();
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object obj) throws IllegalArgumentException {
        String url = ((FilterInvocation) obj).getRequestUrl();
        Iterator<String> ite = resourceMap.keySet().iterator();
        while (ite.hasNext()) {
            String resURL = ite.next();
            if (antPathMatcher.match(url, resURL)) {
                return resourceMap.get(resURL);
            }
        }
        return null;
    }

    /**
     * 加载权限资源
     * @return
     */
    private Map<String, Collection<ConfigAttribute>> loadResourceDefineMain(){
        Map<String, Collection<ConfigAttribute>> newResourceMap = new HashMap<String, Collection<ConfigAttribute>>();
        List<CdResource> cdResourceList = authenticationService.getResourcesAll();
        for (CdResource cdResource: cdResourceList){
            log.debug("获取的资源名称：["+cdResource.getResourceName()+"],资源地址：["+cdResource.getResourceUrl()+"]");
            Collection<ConfigAttribute> atts = new ArrayList<ConfigAttribute>();
            List<CdRole> cdRoleList = authenticationService.getRolesByResourceUrl(cdResource.getResourceUrl());
            for(CdRole cdRole : cdRoleList){
                ConfigAttribute ca = new SecurityConfig(cdRole.getRoleName());
                atts.add(ca);
            }
            newResourceMap.put(cdResource.getResourceUrl(), atts);
        }
        return newResourceMap;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return true;
    }
}
