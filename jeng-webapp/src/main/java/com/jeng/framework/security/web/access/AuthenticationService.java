package com.jeng.framework.security.web.access;

import com.jeng.framework.mybatis.service.BaseMybatisService;
import com.jeng.webapp.model.CdResource;
import com.jeng.webapp.model.CdRole;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.List;

/**
 * 权限认证接口
 */
public interface AuthenticationService extends BaseMybatisService {

    /**
     * 根据用户名查询授权的权限
     * @param username
     * @return
     */
    public Collection<GrantedAuthority> getGrantedAuthority(String username);

    /**
     * 根据用户名查询用户拥有的角色
     * @param username
     * @return
     */
    public List<CdRole> getRolesByUsername(String username);

    /**
     * 根据用户名查询用户拥有的资源
     * @param username
     * @return
     */
    public List<CdResource> getResourcesByUsername(String username);

    /**
     * 根据角色查询所拥有的资源
     * @param cdRole
     * @return
     */
    public List<CdResource> getResourcesByRole(CdRole cdRole);

    /**
     * 查询所有可用的资源权限
     * @return
     */
    public List<CdResource> getResourcesAll();

    /**
     * 根据资源URL查询拥有此权限的角色
     * @param url
     * @return
     */
    public List<CdRole> getRolesByResourceUrl(String url);

    /**
     * 刷新内存资源权限
     */
    public void refreshAccess();
}
