package com.jeng.framework.security.web.access.impl;

import com.jeng.framework.mybatis.service.impl.BaseMybatisServiceImpl;
import com.jeng.framework.security.web.access.AuthenticationService;
import com.jeng.framework.security.web.access.InvocationSecurityMetadataSourceService;
import com.jeng.webapp.model.CdResource;
import com.jeng.webapp.model.CdRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * Created by jengt_000 on 2015/1/17.
 */
@Service
@Transactional
public class AuthenticationServiceImpl extends BaseMybatisServiceImpl implements AuthenticationService {

    @Autowired(required = false)
    InvocationSecurityMetadataSourceService invocationSecurityMetadataSourceService;

    @Override
    public Collection<GrantedAuthority> getGrantedAuthority(String username) {
        List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
        // 所有的用户默认拥有ROLE_USER权限
        log.debug("Grant ROLE_USER to " + username);
        authList.add(new SimpleGrantedAuthority("ROLE_USER"));
        List<CdRole> roleList = getRolesByUsername(username);
        if (roleList.size() > 0) {
            for (CdRole tRole : roleList) {
                log.debug("Grant [ "+tRole.getRoleName()+" ] to [ "+username+" ]");
                authList.add(new SimpleGrantedAuthority(tRole.getRoleName()));
            }
        }
        return authList;
    }

    @Override
    public List<CdRole> getRolesByUsername(String username) {
        Map parameter = new HashMap();
        parameter.put("username",username);
        return mybatisDao.getSqlSessionTemplate().selectList("com.jeng.webapp.mapper.CdResourceMapperExt.selectCdRoleByUsername",
                parameter);
    }

    @Override
    public List<CdResource> getResourcesByUsername(String username) {
        return null;
    }

    @Override
    public List<CdResource> getResourcesByRole(CdRole cdRole) {
        return mybatisDao.getSqlSessionTemplate().selectList("com.jeng.webapp.mapper.CdResourceMapperExt.selectCdResourceByRoleId",
                cdRole);
    }

    @Override
    public List<CdResource> getResourcesAll() {
        CdResource cdResource = new CdResource();
        return mybatisDao.selectByModel(cdResource);
    }

    @Override
    public List<CdRole> getRolesByResourceUrl(String url) {
        CdResource cdResource = new CdResource();
        cdResource.setResourceUrl(url);
        return mybatisDao.getSqlSessionTemplate().selectList("com.jeng.webapp.mapper.CdResourceMapperExt.selectCdResourcesByUrl",
                cdResource);
    }

    @Override
    public void refreshAccess() {
        invocationSecurityMetadataSourceService.loadResourceDefine();
    }
}
