package com.jeng.webapp.service;

import com.jeng.framework.mybatis.service.BaseMybatisService;
import com.jeng.webapp.model.TDepartment;

/**
 * Created by jengt_000 on 2014/12/27.
 */
public interface DemoService extends BaseMybatisService{

    TDepartment insertTestTrancation();
}
