package com.jeng.webapp.service.impl;

import com.jeng.framework.mybatis.service.impl.BaseMybatisServiceImpl;
import com.jeng.webapp.model.TDepartment;
import com.jeng.webapp.model.TDepartmentExample;
import com.jeng.webapp.service.TDepartmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jengt_000 on 2014/12/27.
 */
@Service
@Transactional
public class TDepartmentServiceImpl extends BaseMybatisServiceImpl implements TDepartmentService {

    @Override
    public int countByExample(TDepartmentExample tDepartmentExample) {
        return mybatisDao.countByExample(tDepartmentExample);
    }

    @Override
    public int deleteByExample(TDepartmentExample tDepartmentExample) {
        return mybatisDao.deleteByExample(tDepartmentExample);
    }

    @Override
    public int deleteByPrimaryKey(Class modelClass, Serializable primaryKey) {
        return 0;
    }

    @Override
    public int insertSelective(TDepartment tDepartment) {
        return mybatisDao.insert(tDepartment);
    }

    @Override
    public List<TDepartment> selectByExample(TDepartmentExample tDepartmentExample) {
        return null;
    }

    @Override
    public TDepartment selectByPrimaryKey(Class modelClass, Serializable primaryKey) {
        return null;
    }

    @Override
    public int updateByExampleSelective(TDepartment tDepartment, TDepartmentExample tDepartmentExample) {
        return 0;
    }

    @Override
    public int updateByPrimaryKeySelective(TDepartment tDepartment) {
        return mybatisDao.update(tDepartment);
    }
}
