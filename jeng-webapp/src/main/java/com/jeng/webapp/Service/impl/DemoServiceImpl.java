package com.jeng.webapp.service.impl;

import com.jeng.framework.mybatis.service.impl.BaseMybatisServiceImpl;
import com.jeng.webapp.model.TDepartment;
import com.jeng.webapp.service.DemoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by jengt_000 on 2014/12/27.
 */
@Service
@Transactional
public class DemoServiceImpl extends BaseMybatisServiceImpl implements DemoService {

    @Override
    public TDepartment insertTestTrancation() {
        TDepartment tDepartment = new TDepartment();
        tDepartment.setName("测试事务是否可回滚");
        mybatisDao.insertByModel(tDepartment);
        if(true){
            throw new RuntimeException("事务回滚");
        }
        TDepartment result = mybatisDao.selectOneByModel(tDepartment);
        return result;
    }
}
