package com.jeng.webapp.service;

import com.jeng.webapp.model.TDepartment;
import com.jeng.webapp.model.TDepartmentExample;

import java.io.Serializable;
import java.util.List;

/**
 * Created by jengt_000 on 2014/12/27.
 */
public interface TDepartmentService {

    int countByExample(TDepartmentExample tDepartmentExample);

    int deleteByExample(TDepartmentExample tDepartmentExample);

    int deleteByPrimaryKey(Class modelClass, Serializable primaryKey);

    int insertSelective(TDepartment tDepartment);

    List<TDepartment> selectByExample(TDepartmentExample tDepartmentExample);

    TDepartment selectByPrimaryKey(Class modelClass, Serializable primaryKey);

    int updateByExampleSelective(TDepartment tDepartment, TDepartmentExample tDepartmentExample);

    int updateByPrimaryKeySelective(TDepartment tDepartment);
}
