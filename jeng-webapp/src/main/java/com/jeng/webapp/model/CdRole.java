package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cd_role")
public class CdRole extends BaseMybatisModel {
    /**
     * 
     */
    private Integer cdRoleId;

    /**
     * 角色
     */
    private String roleName;

    /**
     * 角色描述
     */
    private String descpt;

    /**
     * 分类
     */
    private String category;

    @Column(name = "CD_ROLE_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public Integer getCdRoleId() {
        return cdRoleId;
    }

    public void setCdRoleId(Integer cdRoleId) {
        this.cdRoleId = cdRoleId;
        if(cdRoleId == null){
              removeValidField("cdRoleId");
              return;
        }
        addValidField("cdRoleId");
    }

    @Column(name = "ROLE_NAME")
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
        if(roleName == null){
              removeValidField("roleName");
              return;
        }
        addValidField("roleName");
    }

    @Column(name = "DESCPT")
    public String getDescpt() {
        return descpt;
    }

    public void setDescpt(String descpt) {
        this.descpt = descpt;
        if(descpt == null){
              removeValidField("descpt");
              return;
        }
        addValidField("descpt");
    }

    @Column(name = "CATEGORY")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
        if(category == null){
              removeValidField("category");
              return;
        }
        addValidField("category");
    }
}