package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cd_user")
public class CdUser extends BaseMybatisModel {
    /**
     * 
     */
    private Integer cdUserId;

    /**
     * 用户账号
     */
    private String userName;

    /**
     * 姓
     */
    private String firstName;

    /**
     * 名
     */
    private String lastName;

    /**
     * 完整姓名
     */
    private String fullName;

    /**
     * 性别
     */
    private String gender;

    /**
     * 备注
     */
    private String memo;

    /**
     * 关联cd_dept部门表id
     */
    private Integer cdDeptId;

    /**
     * 仓库编码: 关联cd_warehouse仓库表WAREHOUSE_CODE
     */
    private String warehouseCode;

    @Column(name = "CD_USER_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public Integer getCdUserId() {
        return cdUserId;
    }

    public void setCdUserId(Integer cdUserId) {
        this.cdUserId = cdUserId;
        if(cdUserId == null){
              removeValidField("cdUserId");
              return;
        }
        addValidField("cdUserId");
    }

    @Column(name = "USER_NAME")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        if(userName == null){
              removeValidField("userName");
              return;
        }
        addValidField("userName");
    }

    @Column(name = "FIRST_NAME")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
        if(firstName == null){
              removeValidField("firstName");
              return;
        }
        addValidField("firstName");
    }

    @Column(name = "LAST_NAME")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
        if(lastName == null){
              removeValidField("lastName");
              return;
        }
        addValidField("lastName");
    }

    @Column(name = "FULL_NAME")
    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
        if(fullName == null){
              removeValidField("fullName");
              return;
        }
        addValidField("fullName");
    }

    @Column(name = "GENDER")
    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
        if(gender == null){
              removeValidField("gender");
              return;
        }
        addValidField("gender");
    }

    @Column(name = "MEMO")
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
        this.memo = memo;
        if(memo == null){
              removeValidField("memo");
              return;
        }
        addValidField("memo");
    }

    @Column(name = "CD_DEPT_ID")
    public Integer getCdDeptId() {
        return cdDeptId;
    }

    public void setCdDeptId(Integer cdDeptId) {
        this.cdDeptId = cdDeptId;
        if(cdDeptId == null){
              removeValidField("cdDeptId");
              return;
        }
        addValidField("cdDeptId");
    }

    @Column(name = "WAREHOUSE_CODE")
    public String getWarehouseCode() {
        return warehouseCode;
    }

    public void setWarehouseCode(String warehouseCode) {
        this.warehouseCode = warehouseCode;
        if(warehouseCode == null){
              removeValidField("warehouseCode");
              return;
        }
        addValidField("warehouseCode");
    }
}