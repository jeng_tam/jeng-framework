package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cd_resource")
public class CdResource extends BaseMybatisModel {
    /**
     * 
     */
    private Long cdResourceId;

    /**
     * 资源名称
     */
    private String resourceName;

    /**
     * URL
     */
    private String resourceUrl;

    /**
     * 资源排序
     */
    private Short menuOrder;

    /**
     * 资源类别
     */
    private Long menuCate;

    /**
     * 资源描述
     */
    private String descpt;

    /**
     * 使用状态: 0-禁用，1-启用
     */
    private Boolean isActive;

    /**
     * 菜单名称
     */
    private String category;

    @Column(name = "CD_RESOURCE_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public Long getCdResourceId() {
        return cdResourceId;
    }

    public void setCdResourceId(Long cdResourceId) {
        this.cdResourceId = cdResourceId;
        if(cdResourceId == null){
              removeValidField("cdResourceId");
              return;
        }
        addValidField("cdResourceId");
    }

    @Column(name = "RESOURCE_NAME")
    public String getResourceName() {
        return resourceName;
    }

    public void setResourceName(String resourceName) {
        this.resourceName = resourceName;
        if(resourceName == null){
              removeValidField("resourceName");
              return;
        }
        addValidField("resourceName");
    }

    @Column(name = "RESOURCE_URL")
    public String getResourceUrl() {
        return resourceUrl;
    }

    public void setResourceUrl(String resourceUrl) {
        this.resourceUrl = resourceUrl;
        if(resourceUrl == null){
              removeValidField("resourceUrl");
              return;
        }
        addValidField("resourceUrl");
    }

    @Column(name = "MENU_ORDER")
    public Short getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Short menuOrder) {
        this.menuOrder = menuOrder;
        if(menuOrder == null){
              removeValidField("menuOrder");
              return;
        }
        addValidField("menuOrder");
    }

    @Column(name = "MENU_CATE")
    public Long getMenuCate() {
        return menuCate;
    }

    public void setMenuCate(Long menuCate) {
        this.menuCate = menuCate;
        if(menuCate == null){
              removeValidField("menuCate");
              return;
        }
        addValidField("menuCate");
    }

    @Column(name = "DESCPT")
    public String getDescpt() {
        return descpt;
    }

    public void setDescpt(String descpt) {
        this.descpt = descpt;
        if(descpt == null){
              removeValidField("descpt");
              return;
        }
        addValidField("descpt");
    }

    @Column(name = "IS_ACTIVE")
    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
        if(isActive == null){
              removeValidField("isActive");
              return;
        }
        addValidField("isActive");
    }

    @Column(name = "CATEGORY")
    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
        if(category == null){
              removeValidField("category");
              return;
        }
        addValidField("category");
    }
}