package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cd_res_role")
public class CdResRole extends BaseMybatisModel {
    /**
     * 
     */
    private Integer cdResRoleId;

    /**
     * 关联cd_role角色表id
     */
    private Integer cdRoleId;

    /**
     * 关联cd_resource资源表id
     */
    private Integer cdResourceId;

    @Column(name = "CD_RES_ROLE_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public Integer getCdResRoleId() {
        return cdResRoleId;
    }

    public void setCdResRoleId(Integer cdResRoleId) {
        this.cdResRoleId = cdResRoleId;
        if(cdResRoleId == null){
              removeValidField("cdResRoleId");
              return;
        }
        addValidField("cdResRoleId");
    }

    @Column(name = "CD_ROLE_ID")
    public Integer getCdRoleId() {
        return cdRoleId;
    }

    public void setCdRoleId(Integer cdRoleId) {
        this.cdRoleId = cdRoleId;
        if(cdRoleId == null){
              removeValidField("cdRoleId");
              return;
        }
        addValidField("cdRoleId");
    }

    @Column(name = "CD_RESOURCE_ID")
    public Integer getCdResourceId() {
        return cdResourceId;
    }

    public void setCdResourceId(Integer cdResourceId) {
        this.cdResourceId = cdResourceId;
        if(cdResourceId == null){
              removeValidField("cdResourceId");
              return;
        }
        addValidField("cdResourceId");
    }
}