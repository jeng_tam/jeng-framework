package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

@Entity
@Table(name = "t_department")
public class TDepartment extends BaseMybatisModel {
    private String tDepartmentId;

    private String name;

    private String description;

    private String createByUser;

    private Date createDateTime;

    private String createOffice;

    private String createTimeZone;

    private Integer recordVersion;

    private String updateByUser;

    private Date updateDateTime;

    private String updateOffice;

    private String updateTimeZone;

    @Column(name = "T_DEPARTMENT_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public String gettDepartmentId() {
        return tDepartmentId;
    }

    public void settDepartmentId(String tDepartmentId) {
        this.tDepartmentId = tDepartmentId;
        addValidField("tDepartmentId");
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        addValidField("name");
    }

    @Column(name = "DESCRIPTION")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
        addValidField("description");
    }

    @Column(name = "CREATE_BY_USER")
    public String getCreateByUser() {
        return createByUser;
    }

    public void setCreateByUser(String createByUser) {
        this.createByUser = createByUser;
        addValidField("createByUser");
    }

    @Column(name = "CREATE_DATE_TIME")
    public Date getCreateDateTime() {
        return createDateTime;
    }

    public void setCreateDateTime(Date createDateTime) {
        this.createDateTime = createDateTime;
        addValidField("createDateTime");
    }

    @Column(name = "CREATE_OFFICE")
    public String getCreateOffice() {
        return createOffice;
    }

    public void setCreateOffice(String createOffice) {
        this.createOffice = createOffice;
        addValidField("createOffice");
    }

    @Column(name = "CREATE_TIME_ZONE")
    public String getCreateTimeZone() {
        return createTimeZone;
    }

    public void setCreateTimeZone(String createTimeZone) {
        this.createTimeZone = createTimeZone;
        addValidField("createTimeZone");
    }

    @Column(name = "RECORD_VERSION")
    @Version
    public Integer getRecordVersion() {
        return recordVersion;
    }

    public void setRecordVersion(Integer recordVersion) {
        this.recordVersion = recordVersion;
        addValidField("recordVersion");
    }

    @Column(name = "UPDATE_BY_USER")
    public String getUpdateByUser() {
        return updateByUser;
    }

    public void setUpdateByUser(String updateByUser) {
        this.updateByUser = updateByUser;
        addValidField("updateByUser");
    }

    @Column(name = "UPDATE_DATE_TIME")
    public Date getUpdateDateTime() {
        return updateDateTime;
    }

    public void setUpdateDateTime(Date updateDateTime) {
        this.updateDateTime = updateDateTime;
        addValidField("updateDateTime");
    }

    @Column(name = "UPDATE_OFFICE")
    public String getUpdateOffice() {
        return updateOffice;
    }

    public void setUpdateOffice(String updateOffice) {
        this.updateOffice = updateOffice;
        addValidField("updateOffice");
    }

    @Column(name = "UPDATE_TIME_ZONE")
    public String getUpdateTimeZone() {
        return updateTimeZone;
    }

    public void setUpdateTimeZone(String updateTimeZone) {
        this.updateTimeZone = updateTimeZone;
        addValidField("updateTimeZone");
    }
}