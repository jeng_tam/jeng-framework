package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cd_user_role")
public class CdUserRole extends BaseMybatisModel {
    /**
     * 
     */
    private Integer cdUserRoleId;

    /**
     * 关联cd_user后台用户表id
     */
    private Integer cdUserId;

    /**
     * 关联cd_role角色表id
     */
    private Integer cdRoleId;

    @Column(name = "CD_USER_ROLE_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public Integer getCdUserRoleId() {
        return cdUserRoleId;
    }

    public void setCdUserRoleId(Integer cdUserRoleId) {
        this.cdUserRoleId = cdUserRoleId;
        if(cdUserRoleId == null){
              removeValidField("cdUserRoleId");
              return;
        }
        addValidField("cdUserRoleId");
    }

    @Column(name = "CD_USER_ID")
    public Integer getCdUserId() {
        return cdUserId;
    }

    public void setCdUserId(Integer cdUserId) {
        this.cdUserId = cdUserId;
        if(cdUserId == null){
              removeValidField("cdUserId");
              return;
        }
        addValidField("cdUserId");
    }

    @Column(name = "CD_ROLE_ID")
    public Integer getCdRoleId() {
        return cdRoleId;
    }

    public void setCdRoleId(Integer cdRoleId) {
        this.cdRoleId = cdRoleId;
        if(cdRoleId == null){
              removeValidField("cdRoleId");
              return;
        }
        addValidField("cdRoleId");
    }
}