package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisExample;
import java.util.ArrayList;
import java.util.List;

public class CdRoleExample extends BaseMybatisExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CdRoleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCdRoleIdIsNull() {
            addCriterion("CD_ROLE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdIsNotNull() {
            addCriterion("CD_ROLE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID =", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdNotEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID <>", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdGreaterThan(Integer value) {
            addCriterion("CD_ROLE_ID >", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID >=", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdLessThan(Integer value) {
            addCriterion("CD_ROLE_ID <", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdLessThanOrEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID <=", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdIn(List<Integer> values) {
            addCriterion("CD_ROLE_ID in", values, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdNotIn(List<Integer> values) {
            addCriterion("CD_ROLE_ID not in", values, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdBetween(Integer value1, Integer value2) {
            addCriterion("CD_ROLE_ID between", value1, value2, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("CD_ROLE_ID not between", value1, value2, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andRoleNameIsNull() {
            addCriterion("ROLE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andRoleNameIsNotNull() {
            addCriterion("ROLE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andRoleNameEqualTo(String value) {
            addCriterion("ROLE_NAME =", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotEqualTo(String value) {
            addCriterion("ROLE_NAME <>", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameGreaterThan(String value) {
            addCriterion("ROLE_NAME >", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameGreaterThanOrEqualTo(String value) {
            addCriterion("ROLE_NAME >=", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameLessThan(String value) {
            addCriterion("ROLE_NAME <", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameLessThanOrEqualTo(String value) {
            addCriterion("ROLE_NAME <=", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameLike(String value) {
            addCriterion("ROLE_NAME like", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotLike(String value) {
            addCriterion("ROLE_NAME not like", value, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameIn(List<String> values) {
            addCriterion("ROLE_NAME in", values, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotIn(List<String> values) {
            addCriterion("ROLE_NAME not in", values, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameBetween(String value1, String value2) {
            addCriterion("ROLE_NAME between", value1, value2, "roleName");
            return (Criteria) this;
        }

        public Criteria andRoleNameNotBetween(String value1, String value2) {
            addCriterion("ROLE_NAME not between", value1, value2, "roleName");
            return (Criteria) this;
        }

        public Criteria andDescptIsNull() {
            addCriterion("DESCPT is null");
            return (Criteria) this;
        }

        public Criteria andDescptIsNotNull() {
            addCriterion("DESCPT is not null");
            return (Criteria) this;
        }

        public Criteria andDescptEqualTo(String value) {
            addCriterion("DESCPT =", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotEqualTo(String value) {
            addCriterion("DESCPT <>", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptGreaterThan(String value) {
            addCriterion("DESCPT >", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptGreaterThanOrEqualTo(String value) {
            addCriterion("DESCPT >=", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptLessThan(String value) {
            addCriterion("DESCPT <", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptLessThanOrEqualTo(String value) {
            addCriterion("DESCPT <=", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptLike(String value) {
            addCriterion("DESCPT like", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotLike(String value) {
            addCriterion("DESCPT not like", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptIn(List<String> values) {
            addCriterion("DESCPT in", values, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotIn(List<String> values) {
            addCriterion("DESCPT not in", values, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptBetween(String value1, String value2) {
            addCriterion("DESCPT between", value1, value2, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotBetween(String value1, String value2) {
            addCriterion("DESCPT not between", value1, value2, "descpt");
            return (Criteria) this;
        }

        public Criteria andCategoryIsNull() {
            addCriterion("CATEGORY is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIsNotNull() {
            addCriterion("CATEGORY is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryEqualTo(String value) {
            addCriterion("CATEGORY =", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotEqualTo(String value) {
            addCriterion("CATEGORY <>", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryGreaterThan(String value) {
            addCriterion("CATEGORY >", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("CATEGORY >=", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLessThan(String value) {
            addCriterion("CATEGORY <", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLessThanOrEqualTo(String value) {
            addCriterion("CATEGORY <=", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLike(String value) {
            addCriterion("CATEGORY like", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotLike(String value) {
            addCriterion("CATEGORY not like", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryIn(List<String> values) {
            addCriterion("CATEGORY in", values, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotIn(List<String> values) {
            addCriterion("CATEGORY not in", values, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryBetween(String value1, String value2) {
            addCriterion("CATEGORY between", value1, value2, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotBetween(String value1, String value2) {
            addCriterion("CATEGORY not between", value1, value2, "category");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}