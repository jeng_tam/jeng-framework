package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisModel;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cd_user_pwd")
public class CdUserPwd extends BaseMybatisModel {
    /**
     * 
     */
    private Integer cdUserPwdId;

    /**
     * 关联cd_user后台用户表id
     */
    private Integer cdUserId;

    /**
     * 用户名
     */
    private String userName;

    /**
     * 盐值
     */
    private String salt;

    /**
     * 密码
     */
    private String pwd;

    /**
     * 最后登录时间
     */
    private Date lastLoginTime;

    /**
     * 最后登录ip
     */
    private String lastLoginIp;

    /**
     * cookie
     */
    private String cookie;

    @Column(name = "CD_USER_PWD_ID")
    @Id
    @GeneratedValue(generator = "UUIDGenerator")
    public Integer getCdUserPwdId() {
        return cdUserPwdId;
    }

    public void setCdUserPwdId(Integer cdUserPwdId) {
        this.cdUserPwdId = cdUserPwdId;
        if(cdUserPwdId == null){
              removeValidField("cdUserPwdId");
              return;
        }
        addValidField("cdUserPwdId");
    }

    @Column(name = "CD_USER_ID")
    public Integer getCdUserId() {
        return cdUserId;
    }

    public void setCdUserId(Integer cdUserId) {
        this.cdUserId = cdUserId;
        if(cdUserId == null){
              removeValidField("cdUserId");
              return;
        }
        addValidField("cdUserId");
    }

    @Column(name = "USER_NAME")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
        if(userName == null){
              removeValidField("userName");
              return;
        }
        addValidField("userName");
    }

    @Column(name = "SALT")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
        if(salt == null){
              removeValidField("salt");
              return;
        }
        addValidField("salt");
    }

    @Column(name = "PWD")
    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
        if(pwd == null){
              removeValidField("pwd");
              return;
        }
        addValidField("pwd");
    }

    @Column(name = "LAST_LOGIN_TIME")
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
        if(lastLoginTime == null){
              removeValidField("lastLoginTime");
              return;
        }
        addValidField("lastLoginTime");
    }

    @Column(name = "LAST_LOGIN_IP")
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
        if(lastLoginIp == null){
              removeValidField("lastLoginIp");
              return;
        }
        addValidField("lastLoginIp");
    }

    @Column(name = "COOKIE")
    public String getCookie() {
        return cookie;
    }

    public void setCookie(String cookie) {
        this.cookie = cookie;
        if(cookie == null){
              removeValidField("cookie");
              return;
        }
        addValidField("cookie");
    }
}