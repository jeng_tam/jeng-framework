package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisExample;
import java.util.ArrayList;
import java.util.List;

public class CdResourceExample extends BaseMybatisExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CdResourceExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCdResourceIdIsNull() {
            addCriterion("CD_RESOURCE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdIsNotNull() {
            addCriterion("CD_RESOURCE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdEqualTo(Long value) {
            addCriterion("CD_RESOURCE_ID =", value, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdNotEqualTo(Long value) {
            addCriterion("CD_RESOURCE_ID <>", value, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdGreaterThan(Long value) {
            addCriterion("CD_RESOURCE_ID >", value, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdGreaterThanOrEqualTo(Long value) {
            addCriterion("CD_RESOURCE_ID >=", value, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdLessThan(Long value) {
            addCriterion("CD_RESOURCE_ID <", value, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdLessThanOrEqualTo(Long value) {
            addCriterion("CD_RESOURCE_ID <=", value, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdIn(List<Long> values) {
            addCriterion("CD_RESOURCE_ID in", values, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdNotIn(List<Long> values) {
            addCriterion("CD_RESOURCE_ID not in", values, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdBetween(Long value1, Long value2) {
            addCriterion("CD_RESOURCE_ID between", value1, value2, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andCdResourceIdNotBetween(Long value1, Long value2) {
            addCriterion("CD_RESOURCE_ID not between", value1, value2, "cdResourceId");
            return (Criteria) this;
        }

        public Criteria andResourceNameIsNull() {
            addCriterion("RESOURCE_NAME is null");
            return (Criteria) this;
        }

        public Criteria andResourceNameIsNotNull() {
            addCriterion("RESOURCE_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andResourceNameEqualTo(String value) {
            addCriterion("RESOURCE_NAME =", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotEqualTo(String value) {
            addCriterion("RESOURCE_NAME <>", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameGreaterThan(String value) {
            addCriterion("RESOURCE_NAME >", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_NAME >=", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameLessThan(String value) {
            addCriterion("RESOURCE_NAME <", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_NAME <=", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameLike(String value) {
            addCriterion("RESOURCE_NAME like", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotLike(String value) {
            addCriterion("RESOURCE_NAME not like", value, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameIn(List<String> values) {
            addCriterion("RESOURCE_NAME in", values, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotIn(List<String> values) {
            addCriterion("RESOURCE_NAME not in", values, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameBetween(String value1, String value2) {
            addCriterion("RESOURCE_NAME between", value1, value2, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceNameNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_NAME not between", value1, value2, "resourceName");
            return (Criteria) this;
        }

        public Criteria andResourceUrlIsNull() {
            addCriterion("RESOURCE_URL is null");
            return (Criteria) this;
        }

        public Criteria andResourceUrlIsNotNull() {
            addCriterion("RESOURCE_URL is not null");
            return (Criteria) this;
        }

        public Criteria andResourceUrlEqualTo(String value) {
            addCriterion("RESOURCE_URL =", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlNotEqualTo(String value) {
            addCriterion("RESOURCE_URL <>", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlGreaterThan(String value) {
            addCriterion("RESOURCE_URL >", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlGreaterThanOrEqualTo(String value) {
            addCriterion("RESOURCE_URL >=", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlLessThan(String value) {
            addCriterion("RESOURCE_URL <", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlLessThanOrEqualTo(String value) {
            addCriterion("RESOURCE_URL <=", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlLike(String value) {
            addCriterion("RESOURCE_URL like", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlNotLike(String value) {
            addCriterion("RESOURCE_URL not like", value, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlIn(List<String> values) {
            addCriterion("RESOURCE_URL in", values, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlNotIn(List<String> values) {
            addCriterion("RESOURCE_URL not in", values, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlBetween(String value1, String value2) {
            addCriterion("RESOURCE_URL between", value1, value2, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andResourceUrlNotBetween(String value1, String value2) {
            addCriterion("RESOURCE_URL not between", value1, value2, "resourceUrl");
            return (Criteria) this;
        }

        public Criteria andMenuOrderIsNull() {
            addCriterion("MENU_ORDER is null");
            return (Criteria) this;
        }

        public Criteria andMenuOrderIsNotNull() {
            addCriterion("MENU_ORDER is not null");
            return (Criteria) this;
        }

        public Criteria andMenuOrderEqualTo(Short value) {
            addCriterion("MENU_ORDER =", value, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderNotEqualTo(Short value) {
            addCriterion("MENU_ORDER <>", value, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderGreaterThan(Short value) {
            addCriterion("MENU_ORDER >", value, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderGreaterThanOrEqualTo(Short value) {
            addCriterion("MENU_ORDER >=", value, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderLessThan(Short value) {
            addCriterion("MENU_ORDER <", value, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderLessThanOrEqualTo(Short value) {
            addCriterion("MENU_ORDER <=", value, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderIn(List<Short> values) {
            addCriterion("MENU_ORDER in", values, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderNotIn(List<Short> values) {
            addCriterion("MENU_ORDER not in", values, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderBetween(Short value1, Short value2) {
            addCriterion("MENU_ORDER between", value1, value2, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuOrderNotBetween(Short value1, Short value2) {
            addCriterion("MENU_ORDER not between", value1, value2, "menuOrder");
            return (Criteria) this;
        }

        public Criteria andMenuCateIsNull() {
            addCriterion("MENU_CATE is null");
            return (Criteria) this;
        }

        public Criteria andMenuCateIsNotNull() {
            addCriterion("MENU_CATE is not null");
            return (Criteria) this;
        }

        public Criteria andMenuCateEqualTo(Long value) {
            addCriterion("MENU_CATE =", value, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateNotEqualTo(Long value) {
            addCriterion("MENU_CATE <>", value, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateGreaterThan(Long value) {
            addCriterion("MENU_CATE >", value, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateGreaterThanOrEqualTo(Long value) {
            addCriterion("MENU_CATE >=", value, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateLessThan(Long value) {
            addCriterion("MENU_CATE <", value, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateLessThanOrEqualTo(Long value) {
            addCriterion("MENU_CATE <=", value, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateIn(List<Long> values) {
            addCriterion("MENU_CATE in", values, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateNotIn(List<Long> values) {
            addCriterion("MENU_CATE not in", values, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateBetween(Long value1, Long value2) {
            addCriterion("MENU_CATE between", value1, value2, "menuCate");
            return (Criteria) this;
        }

        public Criteria andMenuCateNotBetween(Long value1, Long value2) {
            addCriterion("MENU_CATE not between", value1, value2, "menuCate");
            return (Criteria) this;
        }

        public Criteria andDescptIsNull() {
            addCriterion("DESCPT is null");
            return (Criteria) this;
        }

        public Criteria andDescptIsNotNull() {
            addCriterion("DESCPT is not null");
            return (Criteria) this;
        }

        public Criteria andDescptEqualTo(String value) {
            addCriterion("DESCPT =", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotEqualTo(String value) {
            addCriterion("DESCPT <>", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptGreaterThan(String value) {
            addCriterion("DESCPT >", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptGreaterThanOrEqualTo(String value) {
            addCriterion("DESCPT >=", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptLessThan(String value) {
            addCriterion("DESCPT <", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptLessThanOrEqualTo(String value) {
            addCriterion("DESCPT <=", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptLike(String value) {
            addCriterion("DESCPT like", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotLike(String value) {
            addCriterion("DESCPT not like", value, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptIn(List<String> values) {
            addCriterion("DESCPT in", values, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotIn(List<String> values) {
            addCriterion("DESCPT not in", values, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptBetween(String value1, String value2) {
            addCriterion("DESCPT between", value1, value2, "descpt");
            return (Criteria) this;
        }

        public Criteria andDescptNotBetween(String value1, String value2) {
            addCriterion("DESCPT not between", value1, value2, "descpt");
            return (Criteria) this;
        }

        public Criteria andIsActiveIsNull() {
            addCriterion("IS_ACTIVE is null");
            return (Criteria) this;
        }

        public Criteria andIsActiveIsNotNull() {
            addCriterion("IS_ACTIVE is not null");
            return (Criteria) this;
        }

        public Criteria andIsActiveEqualTo(Boolean value) {
            addCriterion("IS_ACTIVE =", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveNotEqualTo(Boolean value) {
            addCriterion("IS_ACTIVE <>", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveGreaterThan(Boolean value) {
            addCriterion("IS_ACTIVE >", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveGreaterThanOrEqualTo(Boolean value) {
            addCriterion("IS_ACTIVE >=", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveLessThan(Boolean value) {
            addCriterion("IS_ACTIVE <", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveLessThanOrEqualTo(Boolean value) {
            addCriterion("IS_ACTIVE <=", value, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveIn(List<Boolean> values) {
            addCriterion("IS_ACTIVE in", values, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveNotIn(List<Boolean> values) {
            addCriterion("IS_ACTIVE not in", values, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveBetween(Boolean value1, Boolean value2) {
            addCriterion("IS_ACTIVE between", value1, value2, "isActive");
            return (Criteria) this;
        }

        public Criteria andIsActiveNotBetween(Boolean value1, Boolean value2) {
            addCriterion("IS_ACTIVE not between", value1, value2, "isActive");
            return (Criteria) this;
        }

        public Criteria andCategoryIsNull() {
            addCriterion("CATEGORY is null");
            return (Criteria) this;
        }

        public Criteria andCategoryIsNotNull() {
            addCriterion("CATEGORY is not null");
            return (Criteria) this;
        }

        public Criteria andCategoryEqualTo(String value) {
            addCriterion("CATEGORY =", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotEqualTo(String value) {
            addCriterion("CATEGORY <>", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryGreaterThan(String value) {
            addCriterion("CATEGORY >", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryGreaterThanOrEqualTo(String value) {
            addCriterion("CATEGORY >=", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLessThan(String value) {
            addCriterion("CATEGORY <", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLessThanOrEqualTo(String value) {
            addCriterion("CATEGORY <=", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryLike(String value) {
            addCriterion("CATEGORY like", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotLike(String value) {
            addCriterion("CATEGORY not like", value, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryIn(List<String> values) {
            addCriterion("CATEGORY in", values, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotIn(List<String> values) {
            addCriterion("CATEGORY not in", values, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryBetween(String value1, String value2) {
            addCriterion("CATEGORY between", value1, value2, "category");
            return (Criteria) this;
        }

        public Criteria andCategoryNotBetween(String value1, String value2) {
            addCriterion("CATEGORY not between", value1, value2, "category");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}