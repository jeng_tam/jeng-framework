package com.jeng.webapp.model;

import com.jeng.framework.mybatis.model.BaseMybatisExample;
import java.util.ArrayList;
import java.util.List;

public class CdUserRoleExample extends BaseMybatisExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public CdUserRoleExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andCdUserRoleIdIsNull() {
            addCriterion("CD_USER_ROLE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdIsNotNull() {
            addCriterion("CD_USER_ROLE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdEqualTo(Integer value) {
            addCriterion("CD_USER_ROLE_ID =", value, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdNotEqualTo(Integer value) {
            addCriterion("CD_USER_ROLE_ID <>", value, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdGreaterThan(Integer value) {
            addCriterion("CD_USER_ROLE_ID >", value, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("CD_USER_ROLE_ID >=", value, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdLessThan(Integer value) {
            addCriterion("CD_USER_ROLE_ID <", value, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdLessThanOrEqualTo(Integer value) {
            addCriterion("CD_USER_ROLE_ID <=", value, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdIn(List<Integer> values) {
            addCriterion("CD_USER_ROLE_ID in", values, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdNotIn(List<Integer> values) {
            addCriterion("CD_USER_ROLE_ID not in", values, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdBetween(Integer value1, Integer value2) {
            addCriterion("CD_USER_ROLE_ID between", value1, value2, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserRoleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("CD_USER_ROLE_ID not between", value1, value2, "cdUserRoleId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdIsNull() {
            addCriterion("CD_USER_ID is null");
            return (Criteria) this;
        }

        public Criteria andCdUserIdIsNotNull() {
            addCriterion("CD_USER_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCdUserIdEqualTo(Integer value) {
            addCriterion("CD_USER_ID =", value, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdNotEqualTo(Integer value) {
            addCriterion("CD_USER_ID <>", value, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdGreaterThan(Integer value) {
            addCriterion("CD_USER_ID >", value, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("CD_USER_ID >=", value, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdLessThan(Integer value) {
            addCriterion("CD_USER_ID <", value, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdLessThanOrEqualTo(Integer value) {
            addCriterion("CD_USER_ID <=", value, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdIn(List<Integer> values) {
            addCriterion("CD_USER_ID in", values, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdNotIn(List<Integer> values) {
            addCriterion("CD_USER_ID not in", values, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdBetween(Integer value1, Integer value2) {
            addCriterion("CD_USER_ID between", value1, value2, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdUserIdNotBetween(Integer value1, Integer value2) {
            addCriterion("CD_USER_ID not between", value1, value2, "cdUserId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdIsNull() {
            addCriterion("CD_ROLE_ID is null");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdIsNotNull() {
            addCriterion("CD_ROLE_ID is not null");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID =", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdNotEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID <>", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdGreaterThan(Integer value) {
            addCriterion("CD_ROLE_ID >", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID >=", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdLessThan(Integer value) {
            addCriterion("CD_ROLE_ID <", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdLessThanOrEqualTo(Integer value) {
            addCriterion("CD_ROLE_ID <=", value, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdIn(List<Integer> values) {
            addCriterion("CD_ROLE_ID in", values, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdNotIn(List<Integer> values) {
            addCriterion("CD_ROLE_ID not in", values, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdBetween(Integer value1, Integer value2) {
            addCriterion("CD_ROLE_ID between", value1, value2, "cdRoleId");
            return (Criteria) this;
        }

        public Criteria andCdRoleIdNotBetween(Integer value1, Integer value2) {
            addCriterion("CD_ROLE_ID not between", value1, value2, "cdRoleId");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}