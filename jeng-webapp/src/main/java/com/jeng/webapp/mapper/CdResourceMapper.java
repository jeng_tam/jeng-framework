package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.CdResource;
import com.jeng.webapp.model.CdResourceExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CdResourceMapper extends MybatisMapper {
    int countByExample(CdResourceExample example);

    int deleteByExample(CdResourceExample example);

    List<CdResource> selectByExample(CdResourceExample example);

    int updateByExampleSelective(@Param("record") CdResource record, @Param("example") CdResourceExample example);
}