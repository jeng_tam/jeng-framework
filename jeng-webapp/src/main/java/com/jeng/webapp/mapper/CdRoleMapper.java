package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.CdRole;
import com.jeng.webapp.model.CdRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CdRoleMapper extends MybatisMapper {
    int countByExample(CdRoleExample example);

    int deleteByExample(CdRoleExample example);

    List<CdRole> selectByExample(CdRoleExample example);

    int updateByExampleSelective(@Param("record") CdRole record, @Param("example") CdRoleExample example);
}