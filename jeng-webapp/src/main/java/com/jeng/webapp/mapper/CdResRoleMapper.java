package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.CdResRole;
import com.jeng.webapp.model.CdResRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CdResRoleMapper extends MybatisMapper {
    int countByExample(CdResRoleExample example);

    int deleteByExample(CdResRoleExample example);

    List<CdResRole> selectByExample(CdResRoleExample example);

    int updateByExampleSelective(@Param("record") CdResRole record, @Param("example") CdResRoleExample example);
}