package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.CdUserRole;
import com.jeng.webapp.model.CdUserRoleExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CdUserRoleMapper extends MybatisMapper {
    int countByExample(CdUserRoleExample example);

    int deleteByExample(CdUserRoleExample example);

    List<CdUserRole> selectByExample(CdUserRoleExample example);

    int updateByExampleSelective(@Param("record") CdUserRole record, @Param("example") CdUserRoleExample example);
}