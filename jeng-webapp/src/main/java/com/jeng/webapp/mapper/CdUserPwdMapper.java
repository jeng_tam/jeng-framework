package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.CdUserPwd;
import com.jeng.webapp.model.CdUserPwdExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CdUserPwdMapper extends MybatisMapper {
    int countByExample(CdUserPwdExample example);

    int deleteByExample(CdUserPwdExample example);

    List<CdUserPwd> selectByExample(CdUserPwdExample example);

    int updateByExampleSelective(@Param("record") CdUserPwd record, @Param("example") CdUserPwdExample example);
}