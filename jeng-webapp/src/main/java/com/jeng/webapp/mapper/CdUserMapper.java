package com.jeng.webapp.mapper;

import com.jeng.framework.mybatis.mapper.MybatisMapper;
import com.jeng.webapp.model.CdUser;
import com.jeng.webapp.model.CdUserExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface CdUserMapper extends MybatisMapper {
    int countByExample(CdUserExample example);

    int deleteByExample(CdUserExample example);

    List<CdUser> selectByExample(CdUserExample example);

    int updateByExampleSelective(@Param("record") CdUser record, @Param("example") CdUserExample example);
}