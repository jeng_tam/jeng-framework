package com.jeng.webapp.controller;

import com.jeng.framework.security.web.access.AuthenticationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by jengt_000 on 2015/1/18.
 */
@Controller
public class AccessDemoController {

    @Autowired
    AuthenticationService authenticationService;

    @RequestMapping(value = "/login.html")
    public String login(){
        return "login";
    }

    @RequestMapping(value = "/index.html")
    public String index(){
        return "index";
    }

    @RequestMapping(value = "/admin.html")
    public String admin(){
        return "admin";
    }

    @RequestMapping(value = "/denied.html")
    public String denied(){
        return "denied";
    }

    @RequestMapping(value = "/{page}.html")
    public String welcome(@PathVariable(value = "page") String page){
        return page;
    }

    @RequestMapping(value = "/security/refresh")
    public String refresh(){
        authenticationService.refreshAccess();
        return "SUCCESS";
    }
}
