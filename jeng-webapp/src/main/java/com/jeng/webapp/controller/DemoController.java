package com.jeng.webapp.controller;

import com.jeng.framework.mybatis.dao.MybatisDao;
import com.jeng.framework.web.controller.BaseController;
import com.jeng.webapp.model.TDepartment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by jengt_000 on 2014/12/2.
 */
@RestController
public class DemoController extends BaseController{

    @Autowired
    MybatisDao mybatisDao;

    @RequestMapping(value = "/getMenu", method = RequestMethod.GET)
    public List<TDepartment> getMenu(){
        List<TDepartment> result = new ArrayList<TDepartment>();
        for (int i = 0; i < 10; i++) {
            TDepartment td = new TDepartment();
            td.setName("测试权限"+i);
            result.add(td);
        }
        return result;
    }

    @RequestMapping(value = "/notAccess", method = RequestMethod.GET)
    public String notAccess(){
        return "notAccess:OK";
    }

    @RequestMapping(value = "/user/user_list.html", method = RequestMethod.GET)
    public String userList(){
        return "notAccess:OK";
    }

}
