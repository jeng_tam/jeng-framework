package com.jeng.framework.web.handler;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.jeng.framework.exception.BusinessException;
import com.jeng.framework.web.vo.JsonVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

public class SystemExceptionResolver implements HandlerExceptionResolver {

    protected Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public ModelAndView resolveException(HttpServletRequest request,
                                         HttpServletResponse response, Object obj,
                                         Exception exception) {
        if (exception instanceof BusinessException) {
            logger.info(exception.getMessage(), exception);
            JsonVo jsonVo = new JsonVo();
            jsonVo.setMessage(exception.getMessage());
            jsonVo.setStatus(JsonVo.STATUS_BUSINESS_EXCEPTION);
            return new ModelAndView().addObject(jsonVo);
        }
        logger.error(exception.getMessage(), exception);
        JsonVo jsonVo = new JsonVo();
        jsonVo.setMessage("Application System Error");
        jsonVo.setExceptionMsg(exception.getMessage());
        jsonVo.setStatus(JsonVo.STATUS_SYSTEM_EXCEPTION);
        return new ModelAndView().addObject(jsonVo);
    }

}
