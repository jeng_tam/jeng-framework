package com.jeng.framework.web.vo;

import java.io.Serializable;

/**
 * Json统一应答Vo对象
 * @author Jeng
 *
 */
public class JsonVo implements Serializable {

	private static final long serialVersionUID = 5426628546577087825L;

	/**
	 * 状态码：请求成功
	 */
	public static final int STATUS_SUCCESS = 1;
	/**
	 * 状态码：业务异常
	 */
	public static final int STATUS_BUSINESS_EXCEPTION = 2;
	/**
	 * 状态码：系统异常
	 */
	public static final int STATUS_SYSTEM_EXCEPTION = 3;
	
	/**
	 * 信息：默认"请求成功"
	 */
	public static final String MSG_DEFAULT = "request success!";
	
	private int status;
	private Object result;
	private String message;
	private String exceptionMsg;
	private boolean isJsonVo;

	public JsonVo() {
		this.isJsonVo = true;
	}

	public JsonVo(Object result) {
		this.status = STATUS_SUCCESS;
		this.result = result;
		this.isJsonVo = true;
		this.message =  "success";
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getExceptionMsg() {
		return exceptionMsg;
	}

	public void setExceptionMsg(String exceptionMsg) {
		this.exceptionMsg = exceptionMsg;
	}

	public boolean isJsonVo() {
		return isJsonVo;
	}

	public void setJsonVo(boolean isJsonVo) {
		this.isJsonVo = isJsonVo;
	}
}
