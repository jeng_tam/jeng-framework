package com.jeng.framework.web.controller;


import com.jeng.framework.exception.BusinessException;
import com.jeng.framework.log.Logger;
import com.jeng.framework.log.LoggerFactory;
import com.jeng.framework.web.vo.JsonVo;
import org.apache.commons.io.FileUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.multiaction.MultiActionController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@RestController
public class FileUploadController extends MultiActionController {

	public Logger log = LoggerFactory.getLogger(FileUploadController.class);

	@RequestMapping(value = "/file/upload" , method = RequestMethod.POST)
	public JsonVo uploadFrontPic(@RequestParam(value = "fileNames") MultipartFile[] files,
							   HttpServletRequest request,HttpServletResponse response) {
		response.setContentType("text/plain;charset=UTF-8");//防止IE8下进行页面跳转
		//如果只是上传一个文件，则只需要MultipartFile类型接收文件即可，而且无需显式指定@RequestParam注解
		//如果想上传多个文件，那么这里就要用MultipartFile[]类型来接收文件，并且还要指定@RequestParam注解
		//并且上传多个文件时，前台表单中的所有<input type="file"/>的name都应该是myfiles，否则参数里的myfiles无法获取到所有上传的文件
		JsonVo jsonVo = new JsonVo();
		for (int i = 0; i < files.length; i++) {
			MultipartFile file = files[i];
			if(file.isEmpty()){
				log.warn("Upload file is empty.");
			}else{
				log.info("=====================");
				log.info("批量上传文件！");
				log.info("=====================");
				log.info("文件长度: " + file.getSize());
				log.info("文件类型: " + file.getContentType());
				log.info("文件名称: " + file.getName());
				log.info("文件原名: " + file.getOriginalFilename());
				log.info("========================================");
				//此处默认上传至项目WEB-INF/upload路径
				String filePath = request.getSession().getServletContext().getRealPath("/WEB-INF/upload");
				//这里不必处理IO流关闭的问题，因为FileUtils.copyInputStreamToFile()方法内部会自动把用到的IO流关掉
				try {
					//将文件上传至指定路径，如存在文件服务器，则根据固定规则调用文件服务存储接口上传到文件服务器
					FileUtils.copyInputStreamToFile(file.getInputStream(), new File(filePath, file.getOriginalFilename()));
				} catch (IOException e) {
					throw new BusinessException("File upload fail", e);
				}
			}
		}
		return jsonVo;
	}
}
