package com.jeng.framework.web.controller;


import com.jeng.framework.log.Logger;
import com.jeng.framework.log.LoggerFactory;

public class BaseController {

	public Logger log = LoggerFactory.getLogger(BaseController.class);

}
